import { bannerSrcs } from '../../config'
import CollectService from '../../services/collect'
import { Utils } from '../../utils/utils'
import {
  noConsole,
} from '../../config';
import ProductUtils from '../../utils/product'
import { getUpdatePage, getNonResultTips, getDataArrayByPage } from '../../utils/pagination'

const collectService = new CollectService()
const utils = new Utils()
const productUtils = new ProductUtils()
const noResultTips = '没有收藏产品'
const noMoreResultTips = '没有更多收藏产品'

export default {
  namespace: 'myCollection',
  state: {
    collectionList: [],  // 我的收藏列表
    collectionPage: 1,   // 当前收藏列表页码
    collectionNoneResultTips: '',   // 没有数据时的提示语
    collectionNoneResult: false,    // 是否没有数据
    collectionTotal: 0,   // 收藏总数
    collectionLastLoadTime: 0   // 最后加载时间
  },
  effects: {
    // 查询我的收藏产品
    * getMyCollection({ payload }, { call, put, select }) {
      
      // 清除没有数据的标志
      yield put({
        type: 'save', payload: {
          collectionNoneResultTips: '',
          collectionNoneResult: false,
          collectionPage: payload.page,
        }
      });

      const { collectionPage, collectionList } = yield select(state => state.myCollection)
      const { defaultPageSize } = yield select(state => state.common)

      const { current_page, last_page, total, data } = yield call(collectService.getCollection.bind(collectService), collectionPage, defaultPageSize)

      if (Array.isArray(data)) {
        // 检查是否需要更新页数
        let updatePage = getUpdatePage(collectionPage, current_page, last_page, data.length)
        
        // 处理返回的收藏列表数据
        // 提取收藏列表中产品，并把collection_id字段放入产品中
        const products = data.map(item => {
          return {
            collection_id: item.collection_id,
            ...item.product,
          }
        })
        const allProducts = current_page > 1 ? [...collectionList, ...products] : products,

        const update: any = {
          collectionList: allProducts,
          collectionPage: updatePage,
          collectionTotal: total,
        }

        // 计算没有更多数据
        const noResult = getNonResultTips(current_page, data.length, defaultPageSize, noResultTips, noMoreResultTips)
        Object.assign(update, {
          collectionNoneResult: noResult.noneResult,
          collectionNoneResultTips: noResult.noneResultTips,
        })

        // 更新最后加载时间
        update.collectionLastLoadTime = new Date().getTime()

        // 更新产品列表和页数
        yield put({ type: 'save', payload: update });
      }
    },

    // 重新加载我的收藏产品
    * refreshMyCollection({ payload }, { call, put, select }) {
      
      // 清除没有数据的标志
      yield put({
        type: 'save', payload: {
          collectionNoneResultTips: '',
          collectionNoneResult: false,
          collectionPage: 1,
          collectionList: [],
        }
      });

      const { collectionPage, collectionList } = yield select(state => state.myCollection)
      const { defaultPageSize } = yield select(state => state.common)

      const { current_page, last_page, total, data } = yield call(collectService.getCollection.bind(collectService), collectionPage, defaultPageSize)

      if (Array.isArray(data)) {
        // 检查是否需要更新页数
        let updatePage = getUpdatePage(collectionPage, current_page, last_page, data.length)
        
        // 处理返回的收藏列表数据
        const collectArray = getDataArrayByPage(current_page, last_page, defaultPageSize, total, collectionList, data)
        // 提取收藏列表中产品，并把collection_id字段放入产品中
        const products = collectArray.map(item => {
          return {
            collection_id: item.collection_id,
            ...item.product,
          }
        })
        const update: any = {
          collectionList: products,
          collectionPage: updatePage,
          collectionTotal: total,
        }

        // 计算没有更多数据
        const noResult = getNonResultTips(current_page, data.length, defaultPageSize, noResultTips, noMoreResultTips)
        Object.assign(update, {
          collectionNoneResult: noResult.noneResult,
          collectionNoneResultTips: noResult.noneResultTips,
        })

        // 更新最后加载时间
        update.collectionLastLoadTime = new Date().getTime()

        // 更新产品列表和页数
        yield put({ type: 'save', payload: update });
      }
    },
  },
  reducers: {
    // 更新state
    save(state, { payload }) {
      return { ...state, ...payload };
    },
  },
};
