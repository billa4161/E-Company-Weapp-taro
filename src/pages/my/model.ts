import { bannerSrcs } from '../../config'
import ProductService from '../../services/product'
import { noConsole } from '../../config';
import { Utils } from '../../utils/utils'
import ProductUtils from '../../utils/product'


const productService = new ProductService()
const utils = new Utils()
const productUtils = new ProductUtils()

export default {
  namespace: 'my',
  state: {
    shareTotalToday: 0,  // 今日分享数
    shareTotal: 0,       // 分享总数
    pageViewToday: 0,    // 今日浏览量
    pageView: 0,         // 浏览量
    friendToday: 0,      // 今日新增好友
    friendTotal: 0,      // 好友总数
  },
  effects: {
      // 获取统计数据
    * getStatisticsData({ payload }, {call, put, select}) {

      },
  },
  reducers: {
    // 更新state
    save(state, { payload }) {
      return {...state, ...payload};
    },
  },
};
