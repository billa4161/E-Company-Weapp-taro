import { ComponentClass } from 'react'
import Taro, { Component, Config } from '@tarojs/taro'
import { View, Text, OpenData, Image } from '@tarojs/components'
import { connect } from '@tarojs/redux'

import { noConsole } from '../../config'
import { Utils } from '../../utils/utils'
import { User as UserUtils } from '../../utils/user'

import MyStatisticsBox from '../../components/my-statistics-box'

import defaultAvatarImg from '../../asset/images/my-page/user-img.fab803a.png'
import notificationImg from '../../asset/images/step-apply-match/notific.png'
import redImg from '../../asset/images/red-packet/red3.jpg'
import './index.scss'

// #region 书写注意
// 
// 目前 typescript 版本还无法在装饰器模式下将 Props 注入到 Taro.Component 中的 props 属性
// 需要显示声明 connect 的参数类型并通过 interface 的方式指定 Taro.Component 子类的 props
// 这样才能完成类型检查和 IDE 的自动提示
// 使用函数模式则无此限制
// ref: https://github.com/DefinitelyTyped/DefinitelyTyped/issues/20796
//
// #endregion

const utils = new Utils()
const userUtils = new UserUtils()

type PageStateProps = {
  isLoginAlready: boolean,
  userInfo: any,
  shareTotalToday: number,  // 今日分享数
  shareTotal: number,       // 分享总数
  pageViewToday: number,    // 今日浏览量
  pageView: number,         // 浏览量
  friendToday: number,      // 今日新增好友
  friendTotal: number,      // 好友总数
}

type PageDispatchProps = {}

type PageOwnProps = {}

type PageState = {
  // 页面Y轴滚动距离
  scrollTop: number
}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps

interface MyPage {
  props: IProps;
  state: PageState;
}

@connect(({ my, common, loading }) => ({
  ...my,
  ...common,
  ...loading,
}), (dispatch) => ({}))
class MyPage extends Component {

  constructor(props) {
    super(props);
  }

  componentWillReceiveProps(nextProps) {

  }

  componentDidMount() {
  }

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }

  render() {
    const { isLoginAlready, userInfo, shareTotalToday, shareTotal, pageViewToday, pageView, friendToday, friendTotal } = this.props
    const env = process.env.TARO_ENV
    // console.log('env = ', env)
    // console.log('state.searchOption = ', this.state.searchOption)
    // console.log('searchOption = ', searchOption)

    return (
      <View className='my-page-container'>
        {/* 用户信息头部 */}
        <View className="header">
          <View className="avatar">
            <OpenData type='userAvatarUrl' />
            {env === 'h5' ? (<Image src={defaultAvatarImg} />) : ''}
          </View>
          <View className="info">
            <OpenData type='userNickName' />
            {isLoginAlready ? (<Text className="phone">电话：{utils.convertMobileToMaskDisplay(userInfo.mobile)}</Text>) : null}
            {!isLoginAlready ? (<Text onClick={this.goToLogin.bind(this)} className="click-me">点我登录</Text>) : null}
          </View>
        </View>

        {/* 通知盒子 */}
        <View className="notification-box">
          <View className="notification">
            <View className="content">
              <Image className="image" src={notificationImg}></Image>
              <Text className="text">{'恭喜您获得推广活动红包!'}</Text>
            </View>
          </View>
          <View className="img-box" onClick={this.onGetBonus}>
            {/* <Image className="red-packet" src={redImg}></Image>
            <Text className="title"> 点我领取 </Text>
            <Image className="image" src={redImg}></Image> */}
            <Image className="image" src={redImg}></Image>
          </View>
        </View>

        {/* 统计box */}
        <View className="statistics-box">
          <MyStatisticsBox shareTotalToday={shareTotalToday} shareTotal={shareTotal}
            pageViewToday={pageViewToday} pageView={pageView}
            friendToday={friendToday} friendTotal={friendTotal} />
        </View>

        {/* 菜单列表 */}
        <View className="main-list">
          <View className="item" onClick={this.onMyMoney.bind(this)}>
            <Text className="iconfont icon-qianbao2"></Text>
            <Text className="text">我的钱包</Text>
            <Text className="iconfont icon-icon1 right"></Text>
          </View>
          <View className="item" onClick={this.onMyFriend.bind(this)}>
            <Text className="iconfont icon-pengyou"></Text>
            <Text className="text">我的好友</Text>
            <Text className="iconfont icon-icon1 right"></Text>
          </View>
          <View className="item" onClick={this.onMyApply.bind(this)}>
            <Text className="iconfont icon-shenqing"></Text>
            <Text className="text">我的申请</Text>
            <Text className="iconfont icon-icon1 right"></Text>
          </View>
          <View className="item" onClick={this.onMyCollect.bind(this)}>
            <Text className="iconfont icon-shoucang"></Text>
            <Text className="text">我的收藏</Text>
            <Text className="iconfont icon-icon1 right"></Text>
          </View>
          {/* <View className="item">
            <Text className="iconfont icon-erweima"></Text>
            <Text>我的专属二维码</Text>
        </View>  */}
          <View className="item" onClick={this.onMyUserInfo.bind(this)}>
            <Text className="iconfont icon-weirenzheng"></Text>
            <Text className="text">我的认证</Text>
            <Text className="iconfont icon-icon1 right"></Text>
          </View>
        </View>
      </View>
    )
  }

  /**
   * 跳转到登录页
   */
  goToLogin() {
    console.log('goToLogin')
  }

  /**
   * 跳转到我的申请
   */
  onMyApply() {
    const url = '/pages/myApply/index'
    Taro.navigateTo({
      url,
    })
  }

  /**
   * 跳转到我的收藏
   */
  onMyCollect() {
    const url = '/pages/myCollection/index'
    if (userUtils.isLoginValid()) {
      Taro.navigateTo({
        url,
      })
    } else {
      userUtils.checkIsLogin(url)
    }
  }

  /**
   * 跳转到我的信息
   */
  onMyUserInfo() {
    // console.log('onMyUserInfo')
    const url = '/pages/myProfile/index'
    Taro.navigateTo({
      url,
    })
  }

  /**
   * 跳转到我的钱包
   */
  onMyMoney() {
    // console.log('onMyMoney')
    const url = '/pages/myMoney/index'
    Taro.navigateTo({
      url,
    })
  }

  /**
   * 跳转到我的好友
   */
  onMyFriend() {
    const url = '/pages/myFriend/index'
    Taro.navigateTo({
      url,
    })
  }

  /**
   * 跳转到红包活动页
   */
  onGetBonus() {
    const url = '/pages/pinganBonus/index'
    Taro.navigateTo({
      url,
    })
  }
}

// #region 导出注意
//
// 经过上面的声明后需要将导出的 Taro.Component 子类修改为子类本身的 props 属性
// 这样在使用这个子类时 Ts 才不会提示缺少 JSX 类型参数错误
//
// #endregion

export default MyPage as ComponentClass<PageOwnProps, PageState>
