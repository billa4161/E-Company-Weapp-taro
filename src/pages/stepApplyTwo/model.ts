import ProductService from '../../services/product'
import { Utils } from '../../utils/utils'
import Apply from '../../model/apply-form'
import {
  yesOrNoOptions,    
} from '../../config';
import ProductUtils from '../../utils/product'
import { User as UserUtils } from '../../utils/user'

const productUtils = new ProductUtils()
const userUtils = new UserUtils()

const defaultForm: Apply[] = [{
    label: '年龄',
    name: 'age',
    type: 'select',
    options: productUtils.initAgeOptions(),
    toThrousand: true,
    suffix: '周岁',
    placeholder: '请选择您的年龄/周岁',
    isRequire: true,
  }, {
    label: '是否购买商业保险',
    name: 'had_insurance',
    type: 'select',
    mode: 'selector',
    options: yesOrNoOptions,
    rangeKey: 'name',
    optionValueKey: 'value',
    placeholder: '请选择',
  }, {
    label: '是否申请过无抵押贷款',
    name: 'had_mortgage',
    type: 'select',
    options: yesOrNoOptions,
    rangeKey: 'name',
    optionValueKey: 'value',
    placeholder: '请选择',
  }, {
    label: '联系电话',
    name: 'tel',
    type: 'input',
    placeholder: '请填写联系电话',
    isRequire: true,
    validator: [userUtils.phoneNumberValidation]
  },
]
// 表单默认值
const formValue = {
  age: 30,
  tel: userUtils.getUserFromLocalStorage() ? userUtils.getUserFromLocalStorage().mobile : ''
}

export default {
  namespace: 'stepApplyTwo',
  state: {
    form: defaultForm,
    formValue,
    isAgreeProtocol: false,
  },
  effects: {
      
  },
  reducers: {
    // 更新state
    save(state, { payload }) {
      return {...state, ...payload};
    },
    // 更新表单项
    updateFormItem(state, { payload }) {
      const { formValue } = state
      const { formItem } = payload
      
      const formCopy = Object.assign({}, formValue)
      formCopy[formItem.name] = formItem.value
      
      return {...state, formValue: formCopy }
    }
  },
};
