import { ComponentClass } from 'react'
import Taro, { Component, Config } from '@tarojs/taro'
import { View, Image, Text } from '@tarojs/components'
import { connect } from '@tarojs/redux'


import { noConsole } from '../../config'
import { Utils } from '../../utils/utils';

import bannerImg from '../../asset/images/step-apply-result/apply-result-banner.jpg'
import buttonImg from '../../asset/images/apply_button@2x.png'
import successImg from '../../asset/images/step-apply-result/success.png'
import ImageButton from '../../components/img-button'

import './index.scss'

const utils = new Utils()

// #region 书写注意
// 
// 目前 typescript 版本还无法在装饰器模式下将 Props 注入到 Taro.Component 中的 props 属性
// 需要显示声明 connect 的参数类型并通过 interface 的方式指定 Taro.Component 子类的 props
// 这样才能完成类型检查和 IDE 的自动提示
// 使用函数模式则无此限制
// ref: https://github.com/DefinitelyTyped/DefinitelyTyped/issues/20796
//
// #endregion

type PageStateProps = {}

type PageDispatchProps = {
  dispatchRefreshApplyList: () => void,
}

type PageOwnProps = {}

type PageState = {}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps

interface SetpApplyResult {
  props: IProps;
  state: PageState;
}

@connect(({ common, loading }) => ({
  ...common,
  ...loading,
}), (dispatch) => ({
  dispatchRefreshApplyList() {
    dispatch({
      type: 'common/refreshUserApplyList',
      payload: {},
    })
  }
}))
class SetpApplyResult extends Component {

  constructor(props) {
    super(props);
  }

  componentWillReceiveProps(nextProps) { }

  componentWillMount() {
     // 刷新申请列表
     this.props.dispatchRefreshApplyList()
  }

  componentDidMount() { }

  componentWillUnmount() {}

  componentDidShow() { }

  componentDidHide() { }

  render() {
    // const { form, formValue } = this.props
    // console.log('state.searchOption = ', this.state.searchOption)
    // console.log('searchOption = ', searchOption)

    return (<View className="step-apply-result-container">
      <View className="header">
        <Image src={bannerImg}></Image>
      </View>
      <View className="body">
        <Image className="success" src={successImg}></Image>
        <Text className="congratulation">恭喜你，提交成功</Text>
        <View className="tips-box">
          <Text className="tips">请等待工作人员与您联系，</Text>
          <Text className="tips">在此期间请保持电话通畅</Text>
        </View>
        <View className="button-box" onClick={this.onBackToShop}>
          <ImageButton imgSrc={buttonImg} buttonText={'返回贷款商城'}></ImageButton>
        </View>
      </View>
    </View>)
  }

  /**
   * 返回贷款商城方法
   * @param {*} e 
   */
  onBackToShop(e) {
    const url = `/pages/shop/index`

    if (process.env.TARO_ENV === 'weapp') {
      if (utils.isTabUrl(url)) {
        Taro.switchTab({
          url,
        })
      } else {
        Taro.redirectTo({
          url,
        })
      }
    } else {
      Taro.redirectTo({
        url,
      })
    }
  }

}

// #region 导出注意
//
// 经过上面的声明后需要将导出的 Taro.Component 子类修改为子类本身的 props 属性
// 这样在使用这个子类时 Ts 才不会提示缺少 JSX 类型参数错误
//
// #endregion

export default SetpApplyResult as ComponentClass<PageOwnProps, PageState>
