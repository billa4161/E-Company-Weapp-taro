import { noConsole } from '../../config';
import { Utils } from '../../utils/utils'
import ProductUtils from '../../utils/product'


const utils = new Utils()
const productUtils = new ProductUtils()

export default {
  namespace: 'myMoney',
  state: {
    remainingMoney: 0,  // 钱包月
    totalMoney: 0.00,   // 累计奖励
    monthMoney: 0.00,   // 本月奖励
  },
  effects: {
      // 获取统计数据
    * getRemainingMoney({ payload }, {call, put, select}) {
      // 清除没有数据的标志
      yield put({
        type: 'save', payload: {
          remainingMoney: 101.02,
        }
      });
    },
  },
  reducers: {
    // 更新state
    save(state, { payload }) {
      return {...state, ...payload};
    },
  },
};
