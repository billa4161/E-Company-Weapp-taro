import { ComponentClass } from 'react'
import Taro, { Component, Config } from '@tarojs/taro'
import { View, ScrollView, Text, Image } from '@tarojs/components'
import { connect } from '@tarojs/redux'

import { isTimeIntervalValid } from '../../utils/pagination'
import Constants from '../../config/constants'
import { Utils } from '../../utils/utils'
import { User as UserUtils } from '../../utils/user'
import { getNextPage } from '../../utils/pagination'

import ApplyList from '../../components/apply-list'
import TextHeader from '../../components/text-header'

import './index.scss'


// #region 书写注意
// 
// 目前 typescript 版本还无法在装饰器模式下将 Props 注入到 Taro.Component 中的 props 属性
// 需要显示声明 connect 的参数类型并通过 interface 的方式指定 Taro.Component 子类的 props
// 这样才能完成类型检查和 IDE 的自动提示
// 使用函数模式则无此限制
// ref: https://github.com/DefinitelyTyped/DefinitelyTyped/issues/20796
//
// #endregion

const utils = new Utils()
const userUtils = new UserUtils()

type PageStateProps = {
  applyList: any[],
  effects: any,
  applyLastLoadTime: number,  // 最后更新时间
  userApplyList: any[],
  applyNoneResult: boolean,
  applyNoneResultTips: string,
  applyPage: number,    // 当前页
  defaultPageSize: number,
}

type PageDispatchProps = {
  dispatchGet: (param: any) => void,
}

type PageOwnProps = {}

type PageState = {}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps

interface MyApply {
  props: IProps;
  state: PageState;
}

@connect(({ myApply, common, loading }) => ({
  ...myApply,
  ...common,
  ...loading,
}), (dispatch) => ({
  dispatchGet(param: any) {
    dispatch({
      type: 'myApply/getMyApply',
      payload: param,
    })
  },
}))
/**
 * 我的申请页面组件
 */
class MyApply extends Component {

  constructor(props) {
    super(props);
  }

  componentWillReceiveProps(nextProps) {

  }

  componentWillMount() {
    // 检查是否已登录
    if (userUtils.isLoginValid()) {
      console.log('myApply 加载数据')
      // 加载数据
      this.props.dispatchGet({page: 1})
    } else {
      const url = '/pages/myApply/index'
      userUtils.checkIsLogin(url)
    }
  }

  componentDidMount() {

  }

  componentWillUnmount() {

  }

  componentDidShow() {
  }

  componentDidHide() { }

  render() {
    const { applyList, applyNoneResult, applyNoneResultTips, effects } = this.props
    // console.log('state.searchOption = ', this.state.searchOption)
    // console.log('searchOption = ', searchOption)

    return (
      <View className='my-apply-container'>
        
        <View className="header">
          <TextHeader title={'我的申请'} />
        </View>

        <ScrollView
          scrollY
          scrollWithAnimation
          scrollTop={0}
          lowerThreshold={0}
          style='height: 93vh'
          onScrollToLower={this.onLoadMoreApply.bind(this)}>

          <ApplyList dataArray={applyList}
            noneResult={applyNoneResult}
            noneResultTips={applyNoneResultTips}
            loading={effects['myApply/getMyApply']} />

        </ScrollView>
      </View>
    )
  }

  /**
   * 滚动到底部触发，加载更多
   * @param e 
   */
  onLoadMoreApply(e) {
    if (this.props.effects['myApply/getMyApply']) {
      // console.log('加载数据中，return !!!', e)
      return
    }
    if (!isTimeIntervalValid(this.props.applyLastLoadTime)) {
      // console.log('太频繁了，return !!!', e)
      return
    }
    this._loadMoreApply()
  }

  /**
   * 加载更多申请
   * 加载下一页数据
   */
  _loadMoreApply() {
    this.props.dispatchGet({
      page: getNextPage(this.props.applyPage, this.props.defaultPageSize, this.props.applyList.length)
    })
  }
}


// #region 导出注意
//
// 经过上面的声明后需要将导出的 Taro.Component 子类修改为子类本身的 props 属性
// 这样在使用这个子类时 Ts 才不会提示缺少 JSX 类型参数错误
//
// #endregion

export default MyApply as ComponentClass<PageOwnProps, PageState>
