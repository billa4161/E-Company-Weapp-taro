import { noConsole } from '../../config';
import { Utils } from '../../utils/utils'
import { User as UserUtils, User } from '../../utils/user'
import Apply from '../../model/apply-form'

const utils = new Utils()
const userUtils = new UserUtils()

const userInfo = userUtils.getUserFromLocalStorage()

const defaultForm: Apply[] = [{
  label: '姓名',
  name: 'name',
  type: 'input',
  inputType: 'text',
  suffix: '',
  placeholder: '请输入姓名',
  isRequire: true,
}, {
  label: '身份证号',
  name: 'id_card',
  type: 'input',
  inputType: 'idcard',
  placeholder: '请输入身份证号码',
  isRequire: true,
  validator: [userUtils.idCardValidation],
}, {
  label: '联系电话',
  name: 'mobile',
  type: 'input',
  inputType: "number",
  placeholder: '请输入联系电话',
  isRequire: true,
  validator: [userUtils.phoneNumberValidation],
}, {
  label: '所在城市',
  name: 'city',
  type: 'select',
  mode: 'region',
  rangeKey: 'name',
  placeholder: '请选择',
  isRequire: true,
  regionRange: 2,
}]
// 表单默认值
const formValue = {
  name: userInfo ? userInfo.name : '',        // 真实姓名
  mobile: userInfo ? userInfo.mobile : '',    // 联系电话
  city: userInfo ? userInfo.city : [],        // 所在城市
  id_card: userInfo ? userInfo.id_card : '',  // 身份证号
}

export default {
  namespace: 'myProfile',
  state: {
    form: defaultForm,
    formValue,
  },
  effects: {
    // 获取统计数据
    * getUserInfo({ payload }, { call, put, select }) {
      // yield put({
      //   type: 'save', payload: {
      //     remainingMoney: 101.02,
      //   }
      // });
    },
  },
  reducers: {
    // 更新state
    save(state, { payload }) {
      return { ...state, ...payload };
    },
    // 更新表单项
    updateFormItem(state, { payload }) {
      const { formValue } = state
      const { formItem } = payload
      
      const formCopy = Object.assign({}, formValue)
      formCopy[formItem.name] = formItem.value
      
      return {...state, formValue: formCopy }
    },
    // 更新表单
    updateForm(state, { payload }) {
      const { formValue } = state
      const newForm = payload
      
      const formCopy = Object.assign({}, formValue, newForm)
      return {...state, formValue: formCopy }
    }
  },
};
