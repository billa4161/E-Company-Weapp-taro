import { ComponentClass } from 'react'
import Taro, { Component, Config } from '@tarojs/taro'
import { View, Text, OpenData, Image } from '@tarojs/components'
import { connect } from '@tarojs/redux'

import buttonImg from '../../asset/images/apply_button@2x.png'

import TextHeader from '../../components/text-header'
import ApplyForm from '../../components/apply-form'
import ImageButton from '../../components/img-button'

import { noConsole } from '../../config'
import { Utils } from '../../utils/utils'
import { User as UserUtils } from '../../utils/user'

import ApplyFormItem from '../../model/apply-form'

import './index.scss'

// #region 书写注意
// 
// 目前 typescript 版本还无法在装饰器模式下将 Props 注入到 Taro.Component 中的 props 属性
// 需要显示声明 connect 的参数类型并通过 interface 的方式指定 Taro.Component 子类的 props
// 这样才能完成类型检查和 IDE 的自动提示
// 使用函数模式则无此限制
// ref: https://github.com/DefinitelyTyped/DefinitelyTyped/issues/20796
//
// #endregion

const utils = new Utils()
const userUtils = new UserUtils()

type FormItem = {
  name: string,
  value: any,
}

type PageStateProps = {
  form: ApplyFormItem[],
  formValue: any,
}

type PageDispatchProps = {
  dispatchGetUser: () => void,
  dispatchUpdateFormItem: (payload: {
    formItem: FormItem
  }) => void
  dispatchUpdateForm: (form: any) => void,
}

type PageOwnProps = {}

type PageState = {

}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps

interface MyPage {
  props: IProps;
  state: PageState;
}

@connect(({ myProfile, common, loading }) => ({
  ...myProfile,
  ...common,
  ...loading,
}), (dispatch) => ({
  dispatchGetUser() {
    dispatch({
      type: 'myProfile/getUserInfo',
    })
  },
  dispatchUpdateFormItem(payload: { formItem: FormItem }) {
    dispatch({
      type: 'myProfile/updateFormItem',
      payload,
    })
  },
  dispatchUpdateForm(form: any) {
    dispatch({
      type: 'myProfile/updateForm',
      payload: form
    })
  }
}))
class MyPage extends Component {

  constructor(props) {
    super(props);
  }

  componentWillReceiveProps(nextProps) {

  }

  componentWillMount() {

    // 检查是否已登录
    if (userUtils.isLoginValid()) {
      // 加载数据
      this.props.dispatchGetUser()
    } else {
      const url = '/pages/myProfile/index'
      userUtils.checkIsLogin(url)
    }
  }

  componentDidMount() {
  }

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }

  render() {
    const { form, formValue } = this.props

    return (
      <View className='my-profile-container'>

        <View className="header">
          <TextHeader title={'我的认证'} />
        </View>

        <View className="content">
          <ApplyForm form={form} formValue={formValue} dispatchType={'myProfile/updateFormItem'} />
        </View>

        <Text className='tips'>请填写真实信息</Text>

        <View className="button-box" onClick={this.onSaveUserInfo}>
          <ImageButton imgSrc={buttonImg} buttonText={'确认修改'}></ImageButton>
        </View>
      </View >
    )
  }

  /**
   * 保存用户认证信息 
   */
  onSaveUserInfo = () => {
    // console.log('onSaveUserInfo')

    const validatorResult = utils.formValidation(this.props.form, this.props.formValue)
      if (!validatorResult.isValidate) {
        Taro.showToast({
          title: validatorResult.msg,
          icon: 'none',
        })
        return
      }

      const { formValue } = this.props

      // 保存表单信息到local storage
      const oldUserInfo = userUtils.getUserFromLocalStorage()
      // 合并新旧user信息
      const update = Object.assign(oldUserInfo, formValue)
      userUtils.saveUserToLocalStorage(update)

      Taro.showToast({
        title: '修改成功',
        icon: 'success',
      })
  }

  // /**
  //  * 初始化表单数据
  //  */
  // initForm() {
  //   const { formValue } = this.props
  //   const formCopy = Object.assign({}, formValue)

  //   // 从服务器请求信用数据
  //   this.props.dispatchGetUser()

  //   // 联系电话默认使用登录的电话号码
  //   const userInfo = userUtils.getUserFromLocalStorage()
  //   if (userInfo) {
  //     formCopy.tel = userInfo.mobile
  //   }

  //   // 加载缓存
  //   const formCache = productUtils.getApplyFormFromStorage()
  //   if (formCache) {
  //     Object.assign(formCopy, formCache)
  //   }

  //   this.props.dispatchUpdateForm(formCopy)
  // }
}

// #region 导出注意
//
// 经过上面的声明后需要将导出的 Taro.Component 子类修改为子类本身的 props 属性
// 这样在使用这个子类时 Ts 才不会提示缺少 JSX 类型参数错误
//
// #endregion

export default MyPage as ComponentClass<PageOwnProps, PageState>
