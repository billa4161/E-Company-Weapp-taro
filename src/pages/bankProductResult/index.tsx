import { ComponentClass } from 'react'
import Taro, { Component, Config } from '@tarojs/taro'
import { View, Image, Text } from '@tarojs/components'
import { connect } from '@tarojs/redux'


import { noConsole } from '../../config'
import { Utils } from '../../utils/utils';
import ProductService from '../../services/product'

import ImageButton from '../../components/img-button'
import BankDetailHeader from '../../components/bank-detail-header'


import './index.scss'
import buttonImg from '../../asset/images/apply_button@2x.png'
import successImg from '../../asset/images/step-apply-result/success.png'

const utils = new Utils()
const productService = new ProductService()

// #region 书写注意
// 
// 目前 typescript 版本还无法在装饰器模式下将 Props 注入到 Taro.Component 中的 props 属性
// 需要显示声明 connect 的参数类型并通过 interface 的方式指定 Taro.Component 子类的 props
// 这样才能完成类型检查和 IDE 的自动提示
// 使用函数模式则无此限制
// ref: https://github.com/DefinitelyTyped/DefinitelyTyped/issues/20796
//
// #endregion

type PageStateProps = {}

type PageDispatchProps = {
  dispatchRefreshApplyList: () => void,
}

type PageOwnProps = {}

type PageState = {
  product_id: string,   // 产品id
  product: any,         // 产品详情
}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps

interface BankProductApplyResult {
  props: IProps;
  state: PageState;
}

@connect(({ common, loading }) => ({
  ...common,
  ...loading,
}), (dispatch) => ({
  dispatchRefreshApplyList() {
    dispatch({
      type: 'common/refreshUserApplyList',
      payload: {},
    })
  }
}))
class BankProductApplyResult extends Component {

  constructor(props) {
    super(props);
  }

  componentWillReceiveProps(nextProps) { }

  componentWillMount() {
    // 刷新申请列表
    this.props.dispatchRefreshApplyList()

    // 获取url中的参数
    const { product_id } = this.$router.params
    this.setState({
      product_id,
    }, () => {
      // 刷新页面参数
      this.initParam()
    })
  }

  componentDidMount() { }

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }

  render() {
    // const { form, formValue } = this.props
    // console.log('state.searchOption = ', this.state.searchOption)
    // console.log('searchOption = ', searchOption)
    const { product } = this.state

    return (<View className="bank-product-apply-result-container">
      <View className="header">
        <View className="bank-product-apply-header">
          <BankDetailHeader product={product ? product : {}} />
        </View>
      </View>
      <View className="body">
        <Image className="success" src={successImg}></Image>
        <Text className="congratulation">恭喜你，提交成功</Text>
        <View className="tips-box">
          <Text className="tips">请等待工作人员与您联系，</Text>
          <Text className="tips">在此期间请保持电话通畅</Text>
        </View>
        <View className="button-box" onClick={this.onBackToShop}>
          <ImageButton imgSrc={buttonImg} buttonText={'返回贷款商城'}></ImageButton>
        </View>
      </View>
    </View>)
  }

  /**
   * 返回贷款商城方法
   * @param {*} e 
   */
  onBackToShop(e) {
    const url = `/pages/shop/index`

    if (process.env.TARO_ENV === 'weapp') {
      if (utils.isTabUrl(url)) {
        Taro.switchTab({
          url,
        })
      } else {
        Taro.redirectTo({
          url,
        })
      }
    } else {
      Taro.redirectTo({
        url,
      })
    }
  }

  /**
   * 初始化页面参数
   */
  initParam() {
    // 加载产品数据
    const that = this
    utils.showLoadingCenter()
    productService.getBankDetail(this.state.product_id)
      .then(res => {
        utils.hideLoadingCenter()
        if (res.code === 0) {
          that.setState({
            product: res.data
          })
        } else {
          utils.hideLoadingCenter()
          setTimeout(() => {
            Taro.showToast({
              title: res.msg,
              icon: 'none',
            })
          }, 0)
        }
      }).catch(error => {
        utils.hideLoadingCenter()
      })
  }

}

// #region 导出注意
//
// 经过上面的声明后需要将导出的 Taro.Component 子类修改为子类本身的 props 属性
// 这样在使用这个子类时 Ts 才不会提示缺少 JSX 类型参数错误
//
// #endregion

export default BankProductApplyResult as ComponentClass<PageOwnProps, PageState>
