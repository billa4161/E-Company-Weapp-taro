import { ComponentClass } from 'react'
import Taro, { Component, Config } from '@tarojs/taro'
import { View, Text, OpenData, Image } from '@tarojs/components'
import { connect } from '@tarojs/redux'

import { noConsole } from '../../config'
import { Utils } from '../../utils/utils'
import { User as UserUtils } from '../../utils/user'

import logoImg from '../../asset/images/pingan-activity/logo.png'
import drawImg from '../../asset/images/pingan-activity/draw.png'
import ruleImg from '../../asset/images/pingan-activity/rule.png'
import './index.scss'

// #region 书写注意
// 
// 目前 typescript 版本还无法在装饰器模式下将 Props 注入到 Taro.Component 中的 props 属性
// 需要显示声明 connect 的参数类型并通过 interface 的方式指定 Taro.Component 子类的 props
// 这样才能完成类型检查和 IDE 的自动提示
// 使用函数模式则无此限制
// ref: https://github.com/DefinitelyTyped/DefinitelyTyped/issues/20796
//
// #endregion

const utils = new Utils()
const userUtils = new UserUtils()

type PageStateProps = {

}

type PageDispatchProps = {}

type PageOwnProps = {}

type PageState = {
  bonusMsg: string,   // 红包抽取提示
  isDrawFinish: boolean,    // 红包抽取成功
}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps

interface PingAnBonus {
  props: IProps;
  state: PageState;
}

/**
 * 平安红包活动页
 */
@connect(({ common, loading }) => ({
  ...common,
  ...loading,
}), (dispatch) => ({}))
class PingAnBonus extends Component {

  constructor(props) {
    super(props);
  }

  componentWillReceiveProps(nextProps) {

  }

  componentDidMount() {
  }

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }

  render() {
    const { } = this.props
    const { bonusMsg, isDrawFinish } = this.state

    return (
      <View className='pingan-bonus-page-container'>
        <View className="header">
          <Text className="title">
            产品推广红包限时抽取
          </Text>
          {/* <View className="logo-box">
            <Image className="logo" src={logoImg}></Image>
          </View> */}
        </View>

        <View className="content">

          <View className="draw-wrap">
            <Image src={drawImg}></Image>
            {
              bonusMsg ? (<Text className="msg"> {this.state.bonusMsg} </Text>) : (<Image className="logo" src={logoImg}></Image>)
            }
            
            { !isDrawFinish ? (<View className="button-box" onClick={this.onDrawBonus}>
                <View className="button">立即领取</View>
              </View>) : null 
            }
            
            { isDrawFinish ? (<View className="button-box" onClick={this.onGoToMyMoney}>
                <View className="button">返回</View>
              </View>) : null 
            }
          </View>

          <View className="rule-box">
            <Image src={ruleImg}></Image>

            <View className="rule-text">
              <Text className="text">
                1. 活动期间 推广平安税贷产品并帮助客户成功办理，推广人可获得红包抽取资格。登录壹融时代公众号可查看红包抽取资格；
            </Text>
              <Text className="text">
                2. 领取的红包将存入壹融时代公众号的用户钱包，用户可对钱包余额提现，提现金额将进入微信零钱；
            </Text>
              <Text className="text">
                3. 请在活动期间完成奖品的领取，活动结束后将无法领取奖品；
            </Text>
            </View>
          </View>
        </View>
      </View>


    )
  }

  /**
   * 抽取红包的方法
   */
  onDrawBonus = () => {
    
    utils.showLoadingCenter('抽取中')

    setTimeout(() => {
      this.setState({
        bonusMsg: '恭喜您获得10元红包，已存入您的零钱',
        isDrawFinish: true,
      })
      utils.hideLoadingCenter()
    }, 2000)
  }

  /**
   * 跳转到我的钱包
   */
  onGoToMyMoney() {
    Taro.navigateTo({
      url: '/pages/my/index'
    })
  }
}

// #region 导出注意
//
// 经过上面的声明后需要将导出的 Taro.Component 子类修改为子类本身的 props 属性
// 这样在使用这个子类时 Ts 才不会提示缺少 JSX 类型参数错误
//
// #endregion

export default PingAnBonus as ComponentClass<PageOwnProps, PageState>
