import { noConsole } from '../../config';
import { Utils } from '../../utils/utils'
import ProductUtils from '../../utils/product'


const utils = new Utils()
const productUtils = new ProductUtils()

export default {
  namespace: 'myFriend',
  state: {
    levelOneFriends: [],  // 一级好友列表
    levelTwoFriends: [],   // 二级好友列表
    totalAmount: 30,    // 好友总数
    totalAward: 103.01,     // 团队累计奖励
    increaseToday: 12,    // 今日新增好友数
  },
  effects: {
      // 获取一级好友列表
    * getLevelOneFriends({ payload }, {call, put, select}) {
    
    },
  },
  reducers: {
    // 更新state
    save(state, { payload }) {
      return {...state, ...payload};
    },
  },
};
