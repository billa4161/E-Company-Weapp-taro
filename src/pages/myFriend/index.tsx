import { ComponentClass } from 'react'
import Taro, { Component, Config } from '@tarojs/taro'
import { View, Text, OpenData, Image } from '@tarojs/components'
import { connect } from '@tarojs/redux'

import TextHeader from '../../components/text-header'

import { noConsole } from '../../config'
import { Utils } from '../../utils/utils'
import { User as UserUtils } from '../../utils/user'

import './index.scss'

// #region 书写注意
// 
// 目前 typescript 版本还无法在装饰器模式下将 Props 注入到 Taro.Component 中的 props 属性
// 需要显示声明 connect 的参数类型并通过 interface 的方式指定 Taro.Component 子类的 props
// 这样才能完成类型检查和 IDE 的自动提示
// 使用函数模式则无此限制
// ref: https://github.com/DefinitelyTyped/DefinitelyTyped/issues/20796
//
// #endregion

const utils = new Utils()
const userUtils = new UserUtils()

type PageStateProps = {
  levelOneFriends: any[],
  levelTwoFriends: any[],
  totalAmount: number,    // 好友总数
  increaseToday: number,  // 今日新增好友数
  totalAward: number,     // 团队累计奖励
}

type PageDispatchProps = {
  dispatchGetLevelOne: () => void,
}

type PageOwnProps = {}

type PageState = {

}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps

interface MyFriend {
  props: IProps;
  state: PageState;
}

@connect(({ myFriend, common, loading }) => ({
  ...myFriend,
  ...common,
  ...loading,
}), (dispatch) => ({
  dispatchGetLevelOne() {
    dispatch({
      type: 'myFriend/getLevelOneFriends',
    })
  }
}))
/**
 * 我的好友页面
 */
class MyFriend extends Component {

  constructor(props) {
    super(props);
  }

  componentWillReceiveProps(nextProps) {

  }

  componentWillMount() {

    // 检查是否已登录
    if (userUtils.isLoginValid()) {
      // 加载数据
      this.props.dispatchGetLevelOne()
    } else {
      const url = '/pages/myFriend/index'
      userUtils.checkIsLogin(url)
    }
  }

  componentDidMount() {
  }

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }

  render() {
    const { levelOneFriends, levelTwoFriends, totalAmount, totalAward, increaseToday } = this.props

    return (
      <View className='my-money-page-container'>

        <View className="header">
          <TextHeader title={'我的好友'} />
          <View className="total-money-box">
            <View className="money">{totalAward}</View>
            <View className="money-desc">累计团队奖励(元)</View>
          </View>
        </View>

        <View className="content">
          <View className="friend-box">
            <View className="item">
              <View className="label">
                <Text>好友总人数</Text>
              </View>
              <View className="number">
                <Text>{totalAmount}</Text>
              </View>
            </View>
            <View className="item">
              <View className="label">
                <Text>今日新增好友人数</Text>
              </View>
              <View className="number">
                <Text>{increaseToday}</Text>
              </View>
            </View>
          </View>
        </View>

        <View className="main-friend-list">
          <View className="item" onClick={this.onMoneyDetail.bind(this)}>
            <Text className="iconfont icon-yijikehu"></Text>
            <Text className="text">一级好友</Text>
            <Text className="iconfont icon-icon1 right"></Text>
          </View>
          <View className="item" onClick={this.onWithdrawDetail.bind(this)}>
            <Text className="iconfont icon-erjiyonghu"></Text>
            <Text className="text">二级好友</Text>
            <Text className="iconfont icon-icon1 right"></Text>
          </View>
        </View>

      </View>
    )
  }

  /**
   * 奖励明细处理方法
   * @param e 
   */
  onMoneyDetail(e) {
    console.log('onMoneyDetail')
  }

  /**
   * 提现明细处理方法
   * @param e 
   */
  onWithdrawDetail(e) {
    console.log('onWithdrawDetail')
  }
}

// #region 导出注意
//
// 经过上面的声明后需要将导出的 Taro.Component 子类修改为子类本身的 props 属性
// 这样在使用这个子类时 Ts 才不会提示缺少 JSX 类型参数错误
//
// #endregion

export default MyFriend as ComponentClass<PageOwnProps, PageState>
