import { bannerSrcs } from '../../config'
import ProductService from '../../services/product'
import { Utils } from '../../utils/utils'
import {
  depositFilterOptions,
  timeFilterOptions,
  sortFilterOptions,
  productTypeOptions,
  mortgageTypeOptions,
  jobsOptions,
} from '../../config';
import ProductUtils from '../../utils/product'
import { getUpdatePage, getNonResultTips, getDataArrayByPage } from '../../utils/pagination'

const productService = new ProductService()
const utils = new Utils()
const productUtils = new ProductUtils()
const noResultTips = '没有搜索到产品'
const noMoreResultTips = '暂无更多产品'

export default {
  namespace: 'shop',
  state: {
    
    bankSearchOption: {
      deposit: depositFilterOptions[0],
      time_limit: timeFilterOptions[0],
      sort: sortFilterOptions[0],
      product_type: productTypeOptions[0],
      mortgage_type: mortgageTypeOptions[0],
      job: jobsOptions[0],
    },
    bankSearchCondition: {   // 搜索条件
      deposit: '',
      time_limit: '',
      sort: 1,
      product_type: '',
      mortgage_type: '',
      job: '',
    },
    bankProductList: [],     // 银行产品列表
    bankProductPage: 1,      // 银行产品分页下标, 服务器规定默认页数是1
    bankProductTotal: 0,     // 银行产品总数
    bankNoneResultTips: '',      // 银行没有搜索到产品的提示语
    bankNoneResult: false,       // 银行没有数据的标志位
    bankLastLoadTime: 0,     // 银行上次加载的时间，用于判断是否加载过快，毫秒

    fastSearchOption: {
      deposit: depositFilterOptions[0],
      time_limit: timeFilterOptions[0],
      sort: sortFilterOptions[0],
      product_type: productTypeOptions[0],
      mortgage_type: mortgageTypeOptions[0],
      job: jobsOptions[0],
    },
    fastSearchCondition: {   // 快速产品搜索条件
      deposit: '',
      time_limit: '',
      sort: 1,
      product_type: '',
      mortgage_type: '',
      job: '',
    },
    fastProductList: [],     // 快速产品列表
    fastProductPage: 1,      // 快速产品分页下标, 服务器规定默认页数是1
    fastProductTotal: 0,     // 快速产品总数
    fastNoneResultTips: '',      // 快速没有搜索到产品的提示语
    fastNoneResult: false,       // 快速没有数据的标志位
    fastLastLoadTime: 0,     // 快速产品上次加载的时间，用于判断是否加载过快，毫秒
  },
  effects: {
    // 搜索银行产品
    * searchBank({ payload }, { call, put, select }) {

      // 清除没有数据的标志
      yield put({
        type: 'save', payload: {
          bankNoneResultTips: '',
          bankNoneResult: false,
          bankProductPage: payload.page,
        }
      });

      const { bankProductPage, bankProductList } = yield select(state => state.shop)
      const { defaultPageSize } = yield select(state => state.common)

      // let condition = utils.emptyKeyFilter(payload)
      let condition = productUtils.convertToQueryCondition(payload)
      const searchCondition = Object.assign({
        page: bankProductPage,
        page_size: defaultPageSize,
      }, condition)
      const { code, data } = yield call(productService.searchBank.bind(productService), { ...searchCondition })

      if (code === 0) {
        const { current_page, last_page, total } = data

        // 检查是否需要更新页数
        let updatePage = getUpdatePage(searchCondition.page, current_page, last_page, data.data.length)

        const update: any = {
          bankProductList: getDataArrayByPage(current_page, last_page, defaultPageSize, total, bankProductList, data.data),
          bankProductPage: updatePage,
          bankProductTotal: total,
        }

        // 计算没有更多数据
        const noResult = getNonResultTips(current_page, data.data.length, defaultPageSize, noResultTips, noMoreResultTips)
        Object.assign(update, {
          bankNoneResult: noResult.noneResult,
          bankNoneResultTips: noResult.noneResultTips,
        })

        // 更新最后加载时间
        update.bankLastLoadTime = new Date().getTime()

        // 更新产品列表和页数
        yield put({ type: 'save', payload: update });
      }
    },
    // 搜索快速产品
    * searchFast({ payload }, { call, put, select }) {

      // 清除没有数据的标志
      yield put({
        type: 'save', payload: {
          fastNoneResultTips: '',
          fastNoneResult: false,
          fastProductPage: payload.page,
        }
      });

      const { fastProductPage, fastProductList } = yield select(state => state.shop)
      const { defaultPageSize } = yield select(state => state.common)

      // let condition = utils.emptyKeyFilter(payload)
      let condition = productUtils.convertToQueryCondition(payload)
      const searchCondition = Object.assign({
        page: fastProductPage,
        page_size: defaultPageSize,
      }, condition)
      const { code, data } = yield call(productService.searchFast.bind(productService), { ...searchCondition })

      if (code === 0) {
        const { current_page, last_page, total } = data

        // 检查是否需要更新页数
        let updatePage = getUpdatePage(searchCondition.page, current_page, last_page, data.data.length)

        const update: any = {
          fastProductList: getDataArrayByPage(current_page, last_page, defaultPageSize, total, fastProductList, data.data),
          fastProductPage: updatePage,
          fastProductTotal: total,
        }

        // 计算没有更多数据
        const noResult = getNonResultTips(current_page, data.data.length, defaultPageSize, noResultTips, noMoreResultTips)
        Object.assign(update, {
          fastNoneResult: noResult.noneResult,
          fastNoneResultTips: noResult.noneResultTips,
        })

        // 更新最后加载时间
        update.fastLastLoadTime = new Date().getTime()

        // 更新产品列表和页数
        yield put({ type: 'save', payload: update });
      }
    },
  },
  reducers: {
    // 更新state
    save(state, { payload }) {
      return { ...state, ...payload };
    },
    // 更新快速产品搜索条件
    updateFastSearchCondition(state, { payload }) {
      //   const { searchCondition } = state
      const { condition, option } = payload
      //   Object.assign(searchCondition, condition)
      return { ...state, fastSearchCondition: condition, fastSearchOption: option }
    },
    // 更新银行产品搜索条件
    updateBankSearchCondition(state, { payload }) {
      //   const { searchCondition } = state
      const { condition, option } = payload
      //   Object.assign(searchCondition, condition)
      return { ...state, bankSearchCondition: condition, bankSearchOption: option }
    },
  },
};
