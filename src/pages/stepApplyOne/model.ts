import ProductService from '../../services/product'
import { Utils } from '../../utils/utils'
import Apply from '../../model/apply-form'
import {
    fastLoanTimeLimitOptions,
    allJobsOptions,
    yesOrNoOptions,
    hasOrNoOptions,
    houseTypeOptions,
    hasCarOptions,
    creditStatusOptions
  } from '../../config';
import ProductUtils from '../../utils/product'
// import { getUpdatePage, getNonResultTips, } from '../../utils/pagination'

const productService = new ProductService()
const utils = new Utils()

const defaultForm: Apply[] = [{
    label: '贷款额度',
    name: 'loan_limit',
    type: 'input',
    inputType: 'number',
    toThrousand: true,
    suffix: '元',
    placeholder: '请输入贷款额度',
    isRequire: true,
    numberRangeMin: 1000,       // 最小额度是1000
    numberRangeMax: 20000000,   // 最大额度是20000000
    numberRangeMinTips: '额度不能小于%s元',
    numberRangeMaxTips: '额度不能大于%s元',
  }, {
    label: '贷款期限',
    name: 'loan_date_limit',
    type: 'select',
    mode: 'selector',
    options: fastLoanTimeLimitOptions,
    rangeKey: 'name',
    optionValueKey: 'value',
    placeholder: '请选择',
    isRequire: true,
  }, {
    label: '职业身份',
    name: 'indentity',
    type: 'select',
    options: allJobsOptions,
    rangeKey: 'name',
    optionValueKey: 'value',
    placeholder: '请选择',
    isRequire: true,
  }, {
    label: '是否有本地公积金',
    name: 'local_accumulation',
    type: 'select',
    options: hasOrNoOptions,
    rangeKey: 'name',
    optionValueKey: 'value',
    placeholder: '请选择',
  }, {
    label: '是否有本地社保',
    name: 'had_social_security',
    type: 'select',
    options: hasOrNoOptions,
    rangeKey: 'name',
    optionValueKey: 'value',
    placeholder: '请选择',
  }, {
    label: '名下房产类型',
    name: 'house_type',
    type: 'select',
    options: houseTypeOptions,
    rangeKey: 'name',
    optionValueKey: 'value',
    placeholder: '请选择',
  }, {
    label: '名下是否有车',
    name: 'had_car',
    type: 'select',
    options: hasCarOptions,
    rangeKey: 'name',
    placeholder: '请选择',
  }, {
    label: '您的信用情况',
    name: 'credit',
    type: 'select',
    options: creditStatusOptions,
    rangeKey: 'name',
    optionValueKey: 'value',
    placeholder: '请选择',
  }, {
    label: '联系人姓名',
    name: 'name',
    type: 'input',
    placeholder: '请输入联系人姓名',
    isRequire: true,
  }, 
  {
    label: '所在城市',
    name: 'city_code',
    type: 'select',
    mode: 'region',
    // options: utils.getInitRegionsById(),
    rangeKey: 'name',
    placeholder: '请选择',
    isRequire: true,
    regionRange: 2,
  }
]
// 表单默认值
const formValue = {
  loan_limit: 10000,
  loan_date_limit: 6,
  // indentity: '',
  // local_accumulation: '',
  // had_social_security: '',

}

export default {
  namespace: 'stepApplyOne',
  state: {
    form: defaultForm,
    formValue,
  },
  effects: {
      
  },
  reducers: {
    // 更新state
    save(state, { payload }) {
      return {...state, ...payload};
    },
    // 更新表单项
    updateFormItem(state, { payload }) {
      const { formValue } = state
      const { formItem } = payload
      
      const formCopy = Object.assign({}, formValue)
      formCopy[formItem.name] = formItem.value
      
      return {...state, formValue: formCopy }
    }
  },
};
