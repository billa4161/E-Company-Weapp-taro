import { bannerSrcs } from '../../config'
import ProductService from '../../services/product'
import { Utils } from '../../utils/utils'
import {
    depositFilterOptions,
    timeFilterOptions,
    sortFilterOptions,
    productTypeOptions,
    mortgageTypeOptions,
    jobsOptions,
  } from '../../config';
import ProductUtils from '../../utils/product'
import home from '.';
import { getUpdatePage, getNonResultTips, getDataArrayByPage, } from '../../utils/pagination'

const productService = new ProductService()
const utils = new Utils()
const productUtils = new ProductUtils()
const noResultTips = '没有搜索到产品'
const noMoreResultTips = '暂无更多产品'

export default {
  namespace: 'home',
  state: {
    bannerSrcs,          // 首页banner的图片src数组
    searchOption: {
        deposit: depositFilterOptions[0],
        time_limit: timeFilterOptions[0],
        sort: sortFilterOptions[0],
        product_type: productTypeOptions[0],
        mortgage_type: mortgageTypeOptions[0],
        job: jobsOptions[0],
    },
    searchCondition: {   // 搜索条件
      deposit: '',
      time_limit: '',
      sort: 1,
      product_type: '',
      mortgage_type: '',
      job: '',
    },
    productList: [],     // 产品列表
    productPage: 1,      // 产品分页下标, 服务器规定默认页数是1
    productTotal: 0,     // 产品总数
    noneResultTips: '',  // 没有搜索到产品的提示语
    noneResult: false,   // 没有数据的标志位
    lastLoadTime: 0,     // 上次加载的时间，用于判断是否加载过快，毫秒
  },
  effects: {
    // 搜索银行产品
    * searchBank({ payload }, {call, put, select}) {

        // 清除没有数据的标志
        yield put({ type: 'save', payload: {
            noneResultTips: '',
            noneResult: false,
        } });

        const { productPage, productList } = yield select(state => state.home)
        const { defaultPageSize } = yield select(state => state.common)
        
        // let condition = utils.emptyKeyFilter(payload)
        let condition = productUtils.convertToQueryCondition(payload)
        const searchCondition = Object.assign({
            page: productPage,
            page_size: defaultPageSize,
        }, condition)
        const { code, data } = yield call(productService.searchBank.bind(productService), { ...searchCondition })
        
        if (code === 0) {
          const { current_page, last_page, total } = data

          // 检查是否需要更新页数
          let updatePage = getUpdatePage(searchCondition.page, current_page, last_page, data.data.length)
          
          const update:any = {
            productList: getDataArrayByPage(current_page, last_page, defaultPageSize, total, productList, data.data),
            productPage: updatePage,
            productTotal: total,
          }

          // 计算没有更多数据
          const noResult = getNonResultTips(current_page, data.data.length, defaultPageSize, noResultTips, noMoreResultTips)
          Object.assign(update, noResult)

          // 更新最后加载时间
          update.lastLoadTime = new Date().getTime()

          // 更新产品列表和页数
          yield put({ type: 'save', payload: update });
        }
      },
  },
  reducers: {
    // 更新state
    save(state, { payload }) {
      return {...state, ...payload};
    },
    // 更新搜索条件
    updateSearchCondition(state, { payload }) {
    //   const { searchCondition } = state
      const { condition, option } = payload
    //   Object.assign(searchCondition, condition)
      return { ...state, searchCondition: condition, searchOption: option }
    },
  },
};
