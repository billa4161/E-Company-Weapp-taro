import ProductService from '../../services/product'
import { Utils } from '../../utils/utils'
import Apply from '../../model/apply-form'
import {
  noConsole,
} from '../../config';
import ProductUtils from '../../utils/product'
import { User as UserUtils } from '../../utils/user'


const productService = new ProductService()
const productUtils = new ProductUtils()
const userUtils = new UserUtils()

export default {
  namespace: 'stepApplyMatch',
  state: {
    condition: {},  // 产品搜索条件
    productList: [],   // 符合条件的产品列表,
    checkedProducts: [],  // 勾选的产品id数组
  },
  effects: {
    // 搜索快速产品
    * searchFast({ payload }, { call, put, select }) {

      // let condition = utils.emptyKeyFilter(payload)
      let condition = productUtils.convertToQueryCondition(payload)
      const searchCondition = Object.assign({
        page: 1,
        page_size: 10,
      }, condition)
      const { code, data } = yield call(productService.searchFast.bind(productService), { ...searchCondition })

      if (code === 0) {
        const update: any = {
          productList: data.data,
        }
        
        // 更新产品列表和页数
        yield put({ type: 'save', payload: update });
      }
    },
  },
  reducers: {
    // 更新state
    save(state, { payload }) {
      return { ...state, ...payload };
    }
  },
};
