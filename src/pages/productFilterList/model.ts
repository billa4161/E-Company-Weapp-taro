
import ProductService from '../../services/product'
import { Utils } from '../../utils/utils'
import {
  depositFilterOptions,
  timeFilterOptions,
  sortFilterOptions,
  productTypeOptions,
  mortgageTypeOptions,
  jobsOptions,
} from '../../config'
import ProductUtils from '../../utils/product'
import { getUpdatePage, getNonResultTips, getDataArrayByPage } from '../../utils/pagination'

const productService = new ProductService()
const utils = new Utils()
const productUtils = new ProductUtils()
const noResultTips = '没有符合条件的产品'
const noMoreResultTips = '暂无更多产品'

export default {
  namespace: 'productFilterList',
  state: {
    productList: [],  // 产品列表
    productPage: 1,   // 产品列表页码
    productNoneResultTips: '',   // 没有数据时的提示语
    productNoneResult: false,    // 是否没有数据
    productTotal: 0,   // 收藏总数
    productLastLoadTime: 0,   // 最后加载时间
    searchOption: {       // 搜索条件选项
      deposit: depositFilterOptions[0],
      time_limit: timeFilterOptions[0],
      sort: sortFilterOptions[0],
      product_type: productTypeOptions[0],
      mortgage_type: mortgageTypeOptions[0],
      job: jobsOptions[0],
    },
    searchCondition: {   // 快速产品搜索条件
      deposit: '',
      time_limit: '',
      sort: 1,
      product_type: '',
      mortgage_type: '',
      job: '',
    },
    loadProductType: 'bank',   // 加载那种产品
  },
  effects: {
    // 搜索产品
    * searchProduct({ payload }, { call, put, select }) {

      // 清除没有数据的标志
      yield put({
        type: 'save', payload: {
          productNoneResultTips: '',
          productNoneResult: false,
          productPage: payload.page,
        }
      });

      const { productPage, productList, loadProductType } = yield select(state => state.productFilterList)
      const { defaultPageSize } = yield select(state => state.common)

      // let condition = utils.emptyKeyFilter(payload)
      let condition = productUtils.convertToQueryCondition(payload)
      const searchCondition = Object.assign({
        page: productPage,
        page_size: defaultPageSize,
      }, condition)

      // 根据产品类型，加载产品列表
      const { code, data } = yield call(loadProductType === 'bank' ? productService.searchBank.bind(productService) : productService.searchFast.bind(productService), 
      { ...searchCondition })

      if (code === 0) {
        const { current_page, last_page, total } = data

        // 检查是否需要更新页数
        let updatePage = getUpdatePage(searchCondition.page, current_page, last_page, data.data.length)

        const update: any = {
          productList: getDataArrayByPage(current_page, last_page, defaultPageSize, total, productList, data.data),
          productPage: updatePage,
          productTotal: total,
        }

        // 计算没有更多数据
        const noResult = getNonResultTips(current_page, data.data.length, defaultPageSize, noResultTips, noMoreResultTips)
        Object.assign(update, {
          productNoneResult: noResult.noneResult,
          productNoneResultTips: noResult.noneResultTips,
        })

        // 更新最后加载时间
        update.productLastLoadTime = new Date().getTime()

        // 更新产品列表和页数
        yield put({ type: 'save', payload: update });
      }
    },
  },
  reducers: {
    // 更新state
    save(state, { payload }) {
      return { ...state, ...payload };
    },
    // 更新产品搜索条件
    updateSearchCondition(state, { payload }) {
      const { condition, option } = payload
      return { ...state, searchCondition: condition, searchOption: option }
    },
  },
};
