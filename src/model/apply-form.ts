import selectionOption from './select-option'

/**
 * 申请表单项的实体类定义
 */
type Apply = {
  label: string,    // 表单项的label
  name: string,     // 表单的name字段
  isRequire?: boolean    // 是否必填
  type: 'input' | 'select'  // 类型，枚举值 input: 输入值, select: 选项
  options?: selectionOption[] | any[]   // type = select 时才存在，选项数组
  inputType?: 'text' | 'number' | 'digit' | 'password' | 'idcard',   // type = input 时才存在，输入框的输入类型
  mode?: 'time' | 'selector' | 'multiSelector' | 'date' | 'region',      // 选择框的mode参数
  suffix?: string,          // 表单项的尾部装饰
  toThrousand?: boolean,    // 是否转换为千分位显示
  placeholder: string,      // place holder 提示内容
  regionRange?: 1 | 2 | 3,  // 省市区的选项， 枚举值 1: 省 2: 省市 3: 省市区
  validator?: Function[],   // 校验方法数组
  rangeKey?: string,        // picker的字段
  optionValueKey?: string,  // 选项值的key
  numberRangeMin?: number   // 数字输入值范围的最小值 
  numberRangeMax?: number   // 数字输入值范围的最大值
  numberRangeMinTips?: string,
  numberRangeMaxTips?: string,
}
export default Apply