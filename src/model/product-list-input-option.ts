import SelectOption from './select-option';
/**
 * 输入选项定义
 */
type InputOption = {
    deposit: SelectOption,    // 贷款金额
    time_limit: SelectOption  // 贷款期限选项
    sort: SelectOption,   // 排序选项
    product_type: SelectOption,   // 产品(抵押)类型选项
    mortgage_type: SelectOption,  // 抵押类型选项
    job: SelectOption,   // 职业选项
};

export default InputOption;