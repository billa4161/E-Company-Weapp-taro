/**
 * 贷款申请订单定义
 */
type Apply = {
    order_number: number,   // 订单号
    loan_limit: number,     // 贷款金额
    product_title: string,  // 产品名
    product_id: number,     // 产品id
    user_id: number,    // 申请人id
    loan_date_limit: number,    // 申请期限(月)
    indentity: number,      // 申请人身份
    local_accumulation: number, // 是否有公积金 0: 否 1: 是
    house_type: number,     // 房产类型
    had_car: number,    // 是否有车
    credit: number,     // 信用状况
    city_code: string,  // 用户所在城市
    had_insurance: number,      // 是否有商业保险
    had_social_security: number,    // 是否有社保
    had_mortgage: number,   // 是否申请过无抵押贷款
    tel: string,    // 用户手机号
    name: string,   // 用户姓名
    age: number,    // 用户年龄
    loan_type: number,  // 贷款类型 1: 极速贷 2: 银行贷
    access_service: number,   // 提交时必须为1
}
export default Apply
