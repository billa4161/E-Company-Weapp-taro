/**
 * 选项类型定义
 */
type SelectOption = {
    name: string,
    value: string | number,
}
export default SelectOption
