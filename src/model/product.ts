/**
 * 贷款产品定义
 */
type Product = {
    product_id: number,  // 产品id
    title: string,      // 标题
    min_deposit: number,    // 最小额度
    max_deposit: number,    // 最大额度
    min_time_limit: number, // 最小贷款期限
    max_time_limit: number, // 最大贷款期限
    loan_time_type: number, // 放款时间类型
    loan_time: string,  // 放款时间
    product_type?: number,   // 贷款产品类型 房抵贷/车抵贷...
    service_type?: number,   // 贷款服务种类 中介/出借/担保
    mortgage_type: number,   // 抵押类型
    guarantee_type?: number,  // 担保金类型
    charge_money?: string,   // 放款后收费类型
    guarantee: string,      // 担保金值(%)
    disposable_cost_type: number, // 一次性收费类型
    disposable_cost: number,      // 一次性收费(%)
    combination_rate_type: number,  // 月综合费率类型
    combination_rate: string, // 月综合费率 (%)
    rate_order: number,     // 排序序号
    city_code: string,  // 放款城市code 例子："440700,440800,440900,441200,440900,441200,441300,441400,441500,441600",
    logo: string,   // logo图片地址 例子："http://img.erongbank.com/201812/5c0f383f69337.png",
    product_feature: string,    // 产品特性
    apply_for_position: string, // 申请条件
    material_requested: string, // 所需材料
    rate_description: string,   // 利率说明
    apply_count: number,    // 申请数量
    views: number,   // 浏览数
    easy_pass: number,  // 易通过 0: 否 1: 是
    created_at: string, // 创建时间
    updated_at: string, // 更新时间
    collection_id?: string, // 收藏id
}
export default Product
