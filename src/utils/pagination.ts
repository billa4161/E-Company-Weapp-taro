/**
 * 分页数据处理工具类
 */

import { loadTimeInterval } from '../config'

export function isTimeIntervalValid(lastLoadTime: number): boolean {
    let result = true
    const currTime = new Date().getTime()
    if (currTime - lastLoadTime < loadTimeInterval) {
        console.log('加载间隔太短，interval =', currTime - lastLoadTime)
        result = false
    }
    return result
}

/**
 * 加载数据后，计算当前页数并返回
 * @param searchPage 当前搜索页
 * @param currentPage 服务器返回的当前页
 * @param lastPage 服务器返回的最后一页
 * @param dataLength 服务器返回的数据长度
 */
export function getUpdatePage(searchPage: number, currentPage: number, lastPage: number, dataLength: number): number {
    
    // 检查是否需要更新页数
    let updatePage = searchPage - 1 > 0 ? searchPage - 1 : 1
    if (currentPage >= updatePage && currentPage <= lastPage && dataLength > 0) {
      // 判断依据： 当前页 <= 最后一页 && 当前页有数据
      // 否则保持当前页数
      updatePage = currentPage
    } 
    return updatePage
}

/**
 * 计算是否没有数据，返回提示
 * @param currentPage 当前页
 * @param dataLength 服务器返回数据长度
 * @param pageSize 每页数据
 * @param noResultTips 没有数据的标志位
 * @param noMoreResultTips 没有数据的提示
 */
export function getNonResultTips(currentPage: number, dataLength: number, pageSize: number,  noResultTips: string, noMoreResultTips: string): { noneResult: boolean, noneResultTips: string } {
    const result = {
      noneResult: false,
      noneResultTips: '',
    }
    if (currentPage === 1 && dataLength === 0) {
      result.noneResult = true
      result.noneResultTips = noResultTips
    }
    if (currentPage >= 1 &&  dataLength < pageSize) {
      result.noneResult = true
      result.noneResultTips = noMoreResultTips
    }
    return result
}

/**
 * 根据页数返回所有数据数组
 * @param currentPage 当前页
 * @param lastPage  最后一页
 * @param pageSize  每页条数
 * @param total  数据总数
 * @param dataArray 已加载的数据数组
 * @param newDatas 新加载的数据数组
 */
export function getDataArrayByPage(currentPage: number, lastPage: number, pageSize: number, total: number, dataArray: any[], newDatas: any[]) {
  let result = dataArray
  
  // 加载到最后一页的情况
  if (currentPage === lastPage) {
    // 总数 - 已加载数 = 最后一页数据条数， 表示第一次加载最后一页的数据
    if (total - dataArray.length === newDatas.length) {
      // 把 newDatas 放入 dataArray
      result.push(...newDatas)
    } else if (total - dataArray.length > 0 || total - dataArray.length < 0) {
      // 最后一页有新增/删除数据，则把更新的数据放入 dataArray
      const newResult = result.slice(0, ((currentPage - 1) * pageSize)) // 截取第一页到上一页的数据
      newResult.push(...newDatas)
      result = newResult
    }
  } else {
    result =  currentPage > 1 ? [...dataArray, ...newDatas] : newDatas
  }
  return result
}

/**
 * 计算下一页的页数
 * @param currentPage 当前页
 * @param pageSize    每页数据条数
 * @param total       已加载数据总数
 */
export function getNextPage(currentPage: number, pageSize: number, total: number): number {
  // 当前页已加载的数据总数 = 应有的数据总数时，页数加1
  let page = currentPage
  if (pageSize * currentPage === total) {
    page += 1
  }
  return page
}
