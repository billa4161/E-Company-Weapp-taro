import Taro from '@tarojs/taro';
import { userOrderKey } from '../config';

export class Apply {

    /**
     * 保存申请订单信息到本地缓存
     * @param orders 用户订单信息对象
     */
    saveOrderToLocalStorage(orders: any[]) {
        Taro.setStorageSync(userOrderKey, orders);
    }

    /**
     * 从本地缓存中查询用户订单信息
     */
    getOrderFromLocalStorage(): any {
        return Taro.getStorageSync(userOrderKey);
    }
}

export default Apply