import Taro from '@tarojs/taro';
import { userInfoKey, userCollectionKey, userLocationKey } from '../config';
import { checkIDCard } from '../utils/idcard-validate'

export class User {

    /**
     * 保存用户信息到本地缓存
     * @param userInfo 用户信息对象
     */
    saveUserToLocalStorage(userInfo: any) {
        Taro.setStorageSync(userInfoKey, userInfo);
    }

    /**
     * 从本地缓存中查询用户信息
     */
    getUserFromLocalStorage(): any {
        return Taro.getStorageSync(userInfoKey);
    }

    /**
    * 保存用户收藏产品信息到本地缓存
    * @param collections 用户收藏产品信息数组
    */
    saveCollectionToLocalStorage(collections: any[]) {
        Taro.setStorageSync(userCollectionKey, collections);
    }

    /**
     * 从本地缓存中查询用户收藏产品
     */
    getCollectionFromLocalStorage(): any {
        return Taro.getStorageSync(userCollectionKey);
    }

    /**
     * 保存用户位置信息到本地storage
     * @param {*} location 例子： { province: '广东省', city: '佛山', district: '禅城区', province_id: '440000', city_code: '440600', district_code: '440604'}
     */
    saveUserLocationToStorage(location) {
        // 保存用户位置对象
        Taro.setStorageSync(userLocationKey, location)
    }

    /**
     * 从本地storage中获取已保存的用户位置信息对象
     */
    getUserLocationFromStorage() {
        return Taro.getStorageSync(userLocationKey)
    }


    /**
     * 检查用户是否已登录
     * 已登录返回 true
     */
    isLogin() {
        let result = false
        const userInfo = Taro.getStorageSync(userInfoKey)
        if (userInfo) {
            result = true
        }
        return result
    }

    /**
     * 登录是否在有效时间内
     * 有效期内返回 true
     */
    isLoginValid() {
        let result = false
        const userInfo = Taro.getStorageSync(userInfoKey)
        if (userInfo) {
            // expired_in = 7200 表示有效时间是7200s
            const { loginDate, expired_in } = userInfo
            if (loginDate) {
                const loginTime = new Date(loginDate).getTime()
                const currentTime = new Date().getTime()

                // 检查是否在有效登录时间内
                if (currentTime - loginTime < (expired_in * 1000)) {
                    result = true
                }
            }
        }
        return result
    }

    /**
     * 检查用户是否已登录，未登录则跳转到登录页
     * @param {string} redirectTo 登录成功后，跳转到页面的url
     */
    checkIsLogin(redirectTo) {
        const isLogin = this.isLogin()
        const isLoginValid = this.isLoginValid()

        if (!isLogin || !isLoginValid) {
            let title = '用户未登录'
            title = (isLogin && !isLoginValid) ? '登录超时' : title

            // 提示
            Taro.showModal({
                title: title,
                content: '登录后可继续操作',
                showCancel: false,
                success: (res) => {
                    if (res.confirm) {
                        // 跳转到登录页
                        this.goToLoginPage(redirectTo)
                    } else if (res.cancel) {
                        // console.log('用户点击取消')
                    }
                }
            })
        }
    }

    /**
     * 跳转到登录页
     * @param redirectTo  登录成功后跳转到的路由地址
     */
    goToLoginPage(redirectTo: string) {
        const url = `/pages/login/index?redirectTo=${redirectTo ? encodeURIComponent(redirectTo) : ''}`
        Taro.navigateTo({
            url,
        })
    }

    /**
     * 校验手机号
     * @param {String} phone 手机号
     */
    phoneNumberValidation(phone) {
        // 手机号正则表达式
        const telRegex = /^1[34578]\d{9}$/
        const isValidate = telRegex.test(phone)
        return {
            isValidate,
            msg: '手机号格式有误，请检查'
        }
    }

    /**
     * 校验身份证号
     * @param {String} idCode  身份证号
     */
    idCardValidation(idCode) {
        const isValidate = checkIDCard(idCode)
        return {
            isValidate,
            msg: '身份证有误，请检查'
        }
    }

    /**
     * 拼接token并返回
     * @param token 
     */
    getBearToken(token: string): string {
        return `Bearer ${token}`
    }
}