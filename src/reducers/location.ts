import { CHANGE_MY_LOCATION } from '../constants/location'

type UserLocation = {
  myCity: string,   // 用户选择的城市
}

const userLocation: UserLocation = {
  myCity: '全国',
}

const INITIAL_STATE = {
  userLocation,
}

export default function location(state = INITIAL_STATE, action?) {
  switch (action.type) {
    case CHANGE_MY_LOCATION:
      return {
        ...state,
        userLocation: Object.assign(state.userLocation, action.payload),  // 修改用户定位
      }
    default:
      return state
  }
}
