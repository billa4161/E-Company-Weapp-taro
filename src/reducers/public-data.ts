import { BANNER_UPDATE, DEFAULT_PAGE_SIZE_UPDATE } from '../constants/public-data'
import { bannerSrcs, defaultPageSize } from '../config/index'

const INITIAL_STATE = {
  bannerSrcs: bannerSrcs,   // 首页banner的图片src数组
  userApplyList: [],        // 用户已申请的产品列表
  defaultPageSize,
}

export default function publicData (state = INITIAL_STATE, action) {
  switch (action.type) {
    case BANNER_UPDATE:
      return {
        ...state,
        bannerSrcs: action.payload,   // 更新banner图片的src数组
      }
    case DEFAULT_PAGE_SIZE_UPDATE:
      return {
        ...state,
        defaultPageSize: action.payload, // 更新分页数量
      }
     default:
       return state
  }
}
