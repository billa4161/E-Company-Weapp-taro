import { combineReducers } from 'redux'
import counter from './counter'
import location from './location'
import publicData from './public-data'

export default combineReducers({
  counter,
  location,
  publicData,
})
