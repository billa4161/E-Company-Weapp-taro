/**
 * 公共数据处理的 Action
 */
import {
  BANNER_UPDATE,
  DEFAULT_PAGE_SIZE_UPDATE
} from '../constants/public-data'

// 改变主页swiper的banner图片
export const updateBanner = () => {
  return {
    type: BANNER_UPDATE
  }
}

// 改变默认分页数
export const updateDefaultPageSize = () => {
  return {
    type: DEFAULT_PAGE_SIZE_UPDATE
  }
}

