/**
 * 用户定位处理的 Action
 */
import {
  CHANGE_MY_LOCATION,
} from '../constants/location'

// 改变用户当前城市的 action
export const changeMyLocation = () => {
  return {
    type: CHANGE_MY_LOCATION
  }
}

