import { HTTP } from '../utils/http'
import { default as ProductUtils } from '../utils/product'
import { User as UserUtils } from '../utils/user'

const productUtils = new ProductUtils()
const userUtils = new UserUtils()

class ApplyService extends HTTP {

    /**
     * 新增一个订单对象
     * @param {any} order  订单对象
     * @param {any} product 产品对象
     */
    addOrder(order: any, product: any): Promise<any> {
        const userInfo = userUtils.getUserFromLocalStorage()
        const { token } = userInfo

        if (userUtils.isLoginValid()) {
            const param = {
                loan_limit: order.loan_limit,
                product_id: product.product_id,
                loan_date_limit: order.loan_date_limit,
                access_service: 1,
                loan_type: productUtils.getProductLoanType(product),
            }
    
            return this.request({
                url: '/mini/apply/order',
                method: 'POST',
                data: param,
                header: {
                    Authorization: userUtils.getBearToken(token),
                }
            })
        }

        const empty = new Promise((resolve, reject) => {
            resolve()
        })
        return empty
    }

    /**
     * 批量新增订单
     * @param {*} order  订单对象
     * @param {array} productIds 产品对象id数组
     */
    addFastProductOrders(order, productIds: string[]): Promise<any> {
        const userInfo = userUtils.getUserFromLocalStorage()
        const { token } = userInfo

        if (userUtils.isLoginValid()) {
            const param = {
                loan_limit: order.loan_limit,
                product_id: productIds,
                loan_date_limit: order.loan_date_limit,
                access_service: 1,
                loan_type: 1,   // 1表示快速产品
            }
    
            return this.request({
                url: '/mini/apply/order',
                method: 'POST',
                data: param,
                header: {
                    Authorization: userUtils.getBearToken(token),
                }
            })
        }

        const empty = new Promise((resolve, reject) => {
            resolve()
        })
        return empty
    }

    /**
     * 获取当前用户申请的订单
     * @param page 当前页
     * @param page_size 每页条数
     */
    getMyOrder(page:number = 1, page_size:number = 10) {
        const userInfo = userUtils.getUserFromLocalStorage()
        const { token } = userInfo

        if (userUtils.isLoginValid()) {
            return this.request({
                url: '/mini/apply/order',
                data: {
                    page: page,
                    page_size,
                },
                header: {
                    Authorization: userUtils.getBearToken(token),
                }
            })
        }

        const empty = new Promise((resolve, reject) => {
            resolve()
        })
        return empty 
    }
}

export default ApplyService