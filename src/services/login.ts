import { HTTP } from '../utils/http'

class LoginService extends HTTP {
    /**
     * 用户手机验证码登录接口
     * @param {string} mobile 手机号
     * @param {string} code 短信验证码
     */
    login(mobile: string, code: string): Promise<any> {
        // 后台服务的 page 从 1 开始
        return this.request({
            url: `/mini/login`,
            method: 'POST',
            data: {
                mobile,
                code,
            }
        })
    }

    /**
     * 获取产品详情
     * @param {string} mobile  电话号码
     */
    getSmsCode(mobile) {
        return this.request({
            url: '/api/sms/code',
            data: {
                mobile,
            }
        })
    }
}

export default LoginService