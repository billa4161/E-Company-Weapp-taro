import Taro from '@tarojs/taro'

import { qq_map_key, qq_map_sk } from '../config'
import { hex_md5 } from '../utils/md5'

class LocationService {

    /**
     * 查询用户的位置信息
     * @param latitude   经度
     * @param longitude  纬度
     */
    getUserLocation(latitude: string, longitude: string) {
        const url = 'https://apis.map.qq.com/ws/geocoder/v1'
        const path = '/ws/geocoder/v1'
        const sig = hex_md5(`${path}?callback=aaa&key=${qq_map_key}&location=${latitude},${longitude}&output=jsonp${qq_map_sk}`)
        return Taro.request({
            url,
            method: 'GET',
            data: {
              callback: 'aaa',
              key: qq_map_key,
              location: `${latitude},${longitude}`,
              output: 'jsonp',
              sig,
            },
            jsonp: true,
            jsonpCache: true,
        }).then(res => {
            /**
             * res e.g.
             * {"statusCode":200,"data":{"status":0,"message":"query ok","request_id":"1ef7e590-0ff1-11e9-89b2-505dac245c14","result":{"location":{"lat":23.00944,"lng":113.12249},"address":"广东省佛山市禅城区岭南大道北80号","formatted_addresses":{"recommend":"石湾佛山市禅城区人民政府(岭南大道北西)","rough":"石湾佛山市禅城区人民政府(岭南大道北西)"},"address_component":{"nation":"中国","province":"广东省","city":"佛山市","district":"禅城区","street":"岭南大道北","street_number":"岭南大道北80号"},"ad_info":{"nation_code":"156","adcode":"440604","city_code":"156440600","name":"中国,广东省,佛山市,禅城区","location":{"lat":23.009439,"lng":113.12249},"nation":"中国","province":"广东省","city":"佛山市","district":"禅城区"},"address_reference":{"business_area":{"id":"6036384016159","title":"石湾","location":{"lat":23.009365,"lng":113.122505},"_distance":0,"_dir_desc":"内"},"famous_area":{"id":"6036384016159","title":"石湾","location":{"lat":23.009365,"lng":113.122505},"_distance":0,"_dir_desc":"内"},"crossroad":{"id":"1458478","title":"岭南大道北/恒福街(路口)","location":{"lat":23.00882,"lng":113.12307},"_distance":85.6,"_dir_desc":"西北"},"town":{"id":"440604010","title":"石湾镇街道","location":{"lat":23.009439,"lng":113.12249},"_distance":0,"_dir_desc":"内"},"street_number":{"id":"2925044082843959353","title":"岭南大道北80号","location":{"lat":23.009439,"lng":113.12249},"_distance":8.4,"_dir_desc":""},"street":{"id":"5909860573673542631","title":"岭南大道北","location":{"lat":23.009439,"lng":113.12307},"_distance":53.9,"_dir_desc":"西"},"landmark_l1":{"id":"16655281890055370674","title":"印象城购物中心(季华五路店)","location":{"lat":23.010801,"lng":113.121773},"_distance":78.5,"_dir_desc":"东南"},"landmark_l2":{"id":"2925044082843959353","title":"佛山市禅城区人民政府","location":{"lat":23.009365,"lng":113.122505},"_distance":0,"_dir_desc":"内"}}}}}
             */
            // console.log('getUserLocation res = ', JSON.stringify(res))
            return res.data
        }).catch(error => {
           console.error('getUserLocation has error: ', error)
        })
    }

    /**
     * 根据查询用户的位置信息
     * @param ip ip地址
     */
    getUserLocationByIp(ip?: string) {
        const url = 'https://apis.map.qq.com/ws/location/v1/ip'
        const path = '/ws/location/v1/ip'
        const sig = hex_md5(`${path}?callback=aaa&key=${qq_map_key}&output=jsonp${qq_map_sk}`)
        return Taro.request({
            url,
            method: 'GET',
            data: {
              key: qq_map_key,
              output: 'jsonp',
              sig,
              callback:'aaa',
            },
            jsonp: true,
            jsonpCache: true,
        }).then(res => {
            /**
             * res e.g.
             * {"statusCode":200,"data":{"status":0,"message":"query ok","result":{"ip":"113.72.10.193","location":{"lat":23.00944,"lng":113.12249},"ad_info":{"nation":"中国","province":"广东省","city":"佛山市","district":"禅城区","adcode":440604}}}}
             */
            // console.log('getUserLocationByIp res = ', JSON.stringify(res))
            return res.data
        }).catch(error => {
           console.error('getUserLocationByIp has error: ', error)
        })
    }

    /**
     * 把返回的位置信息转换为位置对象
     * @param {*} location  位置对象
     * 例子： { province: "广东省", city: "佛山市", latitude: 23.00944, longitude: 113.12249, adcode: "440604" }
     */
     generateLocationObj(location) {
        const { adcode } = location
        const district_code = adcode
        const city_code = adcode.slice(0, -2) + '00'
        const province_code = adcode.slice(0, -4) + '0000'

        return Object.assign(location, {
            province_code,
            city_code,
            district_code,
        })
    }
}

export default LocationService