import { HTTP } from '../utils/http'

class ProductService extends HTTP {
    /**
     * 搜索银行产品列表
     * @param {any} condition 查询条件 包括分页，查询条件
     */
    searchBank(condition: any): Promise<any> {
        // 后台服务的 page 从 1 开始
        return this.request({
            url:`/mini/product/bank`,
            data: condition
        })
    }

    /**
     * 搜索快速产品列表
     * @param condition 查询条件 包括分页，查询条件
     */
    searchFast(condition: any): Promise<any> {
        // 后台服务的 page 从 1 开始
        return this.request({
            url:`/mini/product/fast`,
            data: condition
        })
    }

    /**
     * 获取银行产品详情
     * @param {String} pid 
     */
    getBankDetail(pid): Promise<any> {
        return this.request({
            url: `/mini/product/bank/${pid}`
        })
    }

    /**
     * 获取快读产品详情
     * @param {String} pid 
     */
    getFastDetail(pid): Promise<any> {
        return this.request({
            url: `/mini/product/fast/${pid}`
        })
    }
}

export default ProductService