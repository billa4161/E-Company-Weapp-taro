import Taro from '@tarojs/taro'

// 腾讯地图key
export const qq_map_key = 'A3XBZ-SV5KU-JSEVM-4E5JK-WC3QE-TQB3M'
// 腾讯地图Secret key
export const qq_map_sk = '11QFBWKjngdk5JsLFzwiZ5I69GUbXYz'

// 请求连接前缀
export const baseUrl = (process.env.NODE_ENV !== 'production' && Taro.getEnv() !== Taro.ENV_TYPE.WEAPP) ? 'http://localhost:8090' : 'https://api.erongbank.com';

// 首页banner图片的默认src数组
import banner1 from '../asset/images/spanner1.jpg';
export const bannerSrcs = [banner1, banner1, banner1];

// 默认分页数量
export const defaultPageSize = 10;

// 输出日志信息
export const noConsole = false;

// 加载的时间间隔不能小于500ms
export const loadTimeInterval = 900

// 用户信息存储在 local storage 的key
export const userInfoKey = 'user_info_key'

// 保存在local storage的申请表单对象key
export const applyFormKey = 'apply_form_key'

// 保存在local storage的用户订单对象key
export const userOrderKey = 'user_apply_key'

// 保存在local storage的用户收藏数据的key
export const userCollectionKey = 'user_collection_key'

// 保存在local storage的用户定位数据key
export const userLocationKey = 'user_location_key'

// tab页的url数组
export const tabUrls = ['pages/home/index', 'pages/shop/index', 'pages/my/index']


export const allJobsOptions =  [{  // 所有职业选项
  value: 1,
  name: '上班族'
}, {
  value: 2,
  name: '个体户'
}, {
  value: 3,
  name: '企业主'
}, {
  value: 4,
  name: '自由职业'
}];

// 是否选项
export const yesOrNoOptions = [{
  value: 1,
  name: '是'
}, {
  value: 0,
  name: '否'
}];

// 有，无选项
export const hasOrNoOptions = [{
  value: 1,
  name: '有'
}, {
  value: 0,
  name: '无'
}];

// 是否有车选项
export const hasCarOptions = [{
  value: 0,
  name: '无车'
}, {
  value: 1,
  name: '准备购买'
}, {
  value: 2,
  name: '名下有车'
}];

// 房产类型选项
export const houseTypeOptions = [{
  value: 0,
  name: '无'
}, {
  value: 1,
  name: '小产权房'
}, {
  value: 2,
  name: '房改/危改房'
}, {
  value: 3,
  name: '商铺'
}, {
  value: 4,
  name: '厂房'
}, {
  value: 5,
  name: '商住两用房'
}, {
  value: 6,
  name: '办公楼'
}, {
  value: 7,
  name: '军产房'
}, {
  value: 8,
  name: '商品住宅'
}, {
  value: 9,
  name: '宅基地/自建房'
}, {
  value: 10,
  name: '有房，但不确认房产类型'
}];

// 信用情况选项
export const creditStatusOptions = [{
  value: 1,
  name: '1年内逾期超过3次或超过90天'
}, {
  value: 2,
  name: '1年内逾期少于3次且少于90天'
}, {
  value: 3,
  name: '无信用卡或贷款'
}, {
  value: 4,
  name: '信用良好，无逾期'
}];

// 快速申请贷款期限选项
export const fastLoanTimeLimitOptions = [{
  value: 3,
  name: '3个月'
}, {
  value: 6,
  name: '6个月'
}, {
  value: 12,
  name: '12个月'
}, {
  value: 24,
  name: '2年'
}, {
  value: 36,
  name: '3年'
}, {
  value: 60,
  name: '5年'
}];

// 产品过滤，额度选项
export const depositFilterOptions = [{
  name: '不限',
  value: ''
}, {
  name: '1万',
  value: 10000
}, {
  name: '5万',
  value: 50000
}, {
  name: '30万',
  value: 300000
}, {
  name: '50万',
  value: 500000
}, {
  name: '100万',
  value: 1000000
}, {
  name: '300万',
  value: 3000000
}]

// 产品过滤，期限选项
export const timeFilterOptions = [{
  name: '不限',
  value: '',
}, {
  name: '1个月',
  value: 1
}, {
  name: '3个月',
  value: 3
}, {
  name: '6个月',
  value: 6
}, {
  name: '1年',
  value: 12
}, {
  name: '1.5年',
  value: 18
}, {
  name: '2年',
  value: 24
}, {
  name: '3年',
  value: 36
}, {
  name: '5年',
  value: 60
}];

// 排序选项
export const sortFilterOptions = [{
  name: '智能排序',
  value: 1
}, {
  name: '易通过',
  value: 2
}, {
  name: '利息最少',
  value: 3
}, {
  name: '放款最快',
  value: 4
}];

// 产品种类选项
export const productTypeOptions = [{
  name: '不限',
  value: ''
}, {
  name: '工薪贷',
  value: 1
}, {
  name: '生意贷',
  value: 2
}, {
  name: '车抵贷',
  value: 3
}, {
  name: '房抵贷',
  value: 4
}];

// 职业身份选项
export const jobsOptions = [{
  name: '不限',
  value: ''
}, {
  value: 1,
  name: '上班族'
}, {
  value: 2,
  name: '个体户'
}, {
  value: 3,
  name: '企业主'
}, {
  value: 4,
  name: '自由职业'
}];

// 抵押类型选项
export const mortgageTypeOptions = [{
  name: '不限',
  value: ''
}, {
  name: '无需抵押',
  value: 1,
}, {
  name: '房产抵押',
  value: 2,
}, {
  name: '车辆抵押',
  value: 3,
}];
