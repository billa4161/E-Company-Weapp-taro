import { ComponentClass } from 'react'
import Taro, { Component, Config } from '@tarojs/taro'
import { View, Text, Image } from '@tarojs/components'

import businessImg from '../../asset/images/home-more-product/product_business.png'
import houseImg from '../../asset/images/home-more-product/product_house.png'
import salaryImg from '../../asset/images/home-more-product/product_salary.png'
import smallImg from '../../asset/images/home-more-product/product_small.png'
import worthIcon from '../../asset/images/home-more-product/worth_icon.png'

import { Utils } from '../../utils/utils'

import './index.scss'

const utils = new Utils()
/**
 * 首页更多产品组件
 */
class HomeMoreProduct extends Component {

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }

  render() {
    return (
      <View className="more-product-container">
        <View className="top">
          <Image src={worthIcon} className="image"></Image>
          <Text className="text">更多产品</Text>
        </View>
        <View className="bottom">
          <View className="row">
            <View className="item"
              data-loadProductType="fast"
              data-deposit={10000}
              onClick={ this.goToProductFilterList.bind(this, {
                load_product_type: 'fast',
                deposit: 10000
              }) }>
              <Image src={smallImg} className="image"></Image>
              <View className="description Text">
                <Text className="name">小额极速贷</Text>
                <Text className="limit">额度：3万以下</Text>
              </View>
            </View>
            <View className="item"
              data-loadProductType="bank"
              data-deposit={50000}
              data-productType={2}
              onClick={ this.goToProductFilterList.bind(this, {
                load_product_type: 'bank',
                deposit: 50000,
                product_type: 2,
              })  }>
              <Image src={businessImg} className="image"></Image>
              <View className="description Text">
                <Text className="name">生意贷款</Text>
                <Text className="limit">额度：3-100万</Text>
              </View>
            </View>
          </View>
          <View className="row">
            <View
              data-loadProductType="bank"
              data-deposit={50000}
              data-productType={1}
              onClick={ this.goToProductFilterList.bind(this, {
                load_product_type: 'bank',
                deposit: 50000,
                product_type: 1,
              }) }
              className="item">
              <Image src={salaryImg} className="image"></Image>
              <View className="description Text">
                <Text className="name">工薪贷款</Text>
                <Text className="limit">额度：3-50万</Text>
              </View>
            </View>
            <View className="item"
              data-loadProductType="bank"
              data-deposit={300000}
              data-productType={4}
              onClick={ this.goToProductFilterList.bind(this, {
                load_product_type: 'bank',
                deposit: 300000,
                product_type: 4,
              }) }>
              <Image src={houseImg} className="image"></Image>
              <View className="description Text">
                <Text className="name">房屋贷款</Text>
                <Text className="limit">额度：30万以上</Text>
              </View>
            </View>
          </View>
        </View>
      </View>)
  }

  /**
   * 跳转到产品过滤列表页
   * @param param 参数
   * @param e 事件
   */
  goToProductFilterList(param: any, e) {
    // console.log('goToProductFilterList e = ', e)
    // console.log('goToProductFilterList param = ', param)
    const productFilterUrl = '/pages/productFilterList/index'
    Taro.navigateTo({
      url: utils.getUrlWithParam(productFilterUrl, param)
    })
  }
}

// #region 导出注意
//
// 经过上面的声明后需要将导出的 Taro.Component 子类修改为子类本身的 props 属性
// 这样在使用这个子类时 Ts 才不会提示缺少 JSX 类型参数错误
//
// #endregion

export default HomeMoreProduct as ComponentClass
