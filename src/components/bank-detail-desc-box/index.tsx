import { ComponentClass } from 'react'
import Taro, { Component } from '@tarojs/taro'
import { View, Text, Image } from '@tarojs/components'
import { connect } from '@tarojs/redux'

import { Utils } from '../../utils/utils'

import './index.scss'

const utils = new Utils()

type PageStateProps = {}

type PageDispatchProps = {}

type PageOwnProps = {
  title: string,    // 描述标题
  descText: string, // 描述信息
}

type PageState = {}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps

interface BankDetailDescBox {
  props: IProps;
  state: PageState;
}

/**
 * 银行产品详情描述box组件
 */
@connect(({ }) => ({}), (dispatch) => ({}))
class BankDetailDescBox extends Component {

  constructor(props) {
    super(props);

    this.state = {
      isOpen: false
    }
  }

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }

  render() {
    const { title, descText } = this.props

    let descRows
    if (descText && descText.length > 0) {
      const rowArr = descText.split('\n')
      descRows = rowArr.map((item, index) => {
        return (<Text key={index} className="text" decode={false}>
          { item }
      </Text>)
      })
    }

    return (<View className="bank-detail-desc-box-container">
      <View className="desc-title">
        <Text className="text">{title}</Text>
      </View>
      <View className="desc-content">
        {descRows}
      </View>
    </View>)
  }
}

export default BankDetailDescBox as ComponentClass<PageOwnProps, PageState>
