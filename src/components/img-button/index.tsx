import { ComponentClass } from 'react'
import Taro, { Component } from '@tarojs/taro'
import { View, Text, Image, Button } from '@tarojs/components'

import './index.scss'

type PageStateProps = {}

type PageDispatchProps = {}

type PageOwnProps = {
  imgSrc: any,   // 图片路径对象
  buttonText: string, // 按钮文字
}

type PageState = {}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps

interface ImageButton {
  props: IProps;
}

/**
 * 图片按钮组件
 */
class ImageButton extends Component {

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }

  render() {
    return (
      <Button className='img-button'>
        { this.props.imgSrc && <Image className='bg-img' src={this.props.imgSrc} /> }
        <Text className='img-btn-text'>{this.props.buttonText}</Text>
      </Button>
    )
  }
}

// #region 导出注意
//
// 经过上面的声明后需要将导出的 Taro.Component 子类修改为子类本身的 props 属性
// 这样在使用这个子类时 Ts 才不会提示缺少 JSX 类型参数错误
//
// #endregion

export default ImageButton as ComponentClass<PageOwnProps, PageState>
