import { ComponentClass } from 'react'
import Taro, { Component } from '@tarojs/taro'
import { View, Text, Image } from '@tarojs/components'
import { connect } from '@tarojs/redux'

import { Utils } from '../../utils/utils'

import './index.scss'

const utils = new Utils()

type PageStateProps = {}

type PageDispatchProps = {}

type PageOwnProps = {
  shareTotalToday: number,  // 今日分享数
  shareTotal: number,       // 分享总数
  pageViewToday: number,    // 今日浏览量
  pageView: number,         // 浏览量
  friendToday: number,      // 今日新增好友
  friendTotal: number,      // 好友总数
}

type PageState = {}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps

interface MyStatisticsBox {
  props: IProps;
  state: PageState;
}

/**
 * 我的页面的数据统计box组件
 */
@connect(({ }) => ({}), (dispatch) => ({}))
class MyStatisticsBox extends Component {

  constructor(props) {
    super(props);

    this.state = {
      isOpen: false
    }
  }

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }

  render() {
    const { shareTotalToday, shareTotal, pageViewToday, pageView, friendToday, friendTotal, } = this.props

    return (<View className="my-statistics-box-container">
      {/* 分享数 */}
      <View className="column-item">
        <View className="row">
          <Text className="label">今日分享数:</Text>
          <Text className="number">{shareTotalToday}</Text>
        </View>
        <View className="row">
          <Text className="label">分享总数:</Text>
          <Text className="number">{shareTotal}</Text>
        </View>
      </View>
      {/* 浏览量 */}
      <View className="column-item">
        <View className="row">
          <Text className="label">今日浏览量:</Text>
          <Text className="number">{pageViewToday}</Text>
        </View>
        <View className="row">
          <Text className="label">分享总数:</Text>
          <Text className="number">{pageView}</Text>
        </View>
      </View>
      {/* 好友数 */}
      <View className="column-item">
        <View className="row">
          <Text className="label">今日新增好友:</Text>
          <Text className="number">{friendToday}</Text>
        </View>
        <View className="row">
          <Text className="label">好友总数:</Text>
          <Text className="number">{friendTotal}</Text>
        </View>
      </View>
    </View>)
  }
}

export default MyStatisticsBox as ComponentClass<PageOwnProps, PageState>
