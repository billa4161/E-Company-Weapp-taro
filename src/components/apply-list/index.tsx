import { ComponentClass } from 'react'
import Taro, { Component } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'

import ApplyListItem  from '../apply-list-item'
import Loading from '../loading'

import Apply from '../../model/apply'

import './index.scss'

type PageStateProps = {
  
}

type PageDispatchProps = {}

type PageOwnProps = {
  // 申请列表
  dataArray: Apply[],
  // 加载中
  loading: boolean,
  // 没有加载到数据的标志位
  noneResult: boolean,
  // 没有加载到数据的提示语
  noneResultTips: string,
}

type PageState = {
  
}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps

interface ApplyList {
  props: IProps;
  state: PageState;
}

/**
 * 申请列表组件
 * 只有展示数据的功能
 */
class ApplyList extends Component {

  constructor(props) {
    super(props);

    this.state = {
      
    }
  }

  componentWillReceiveProps(nextProps) {

  }

  componentWillUnmount() {

  }

  componentDidShow() { }

  componentDidHide() { }

  render() {
    const { dataArray } = this.props

    let applyItems: any[] = []
    if (dataArray) {
      applyItems = dataArray.map((item) => {
        return <View key={item.product_id} className='apply-item'>
          <ApplyListItem 
            apply={item}/>
        </View>
      }) 
    }
    return (
      <View className="apply-list-container">
        <View className="applies-container">
          { applyItems }
        </View>
        <View className='bottom-box'>
          { 
            this.props.loading && 
            <View className="loading">
                <Loading />
                <Text className='text'>加载中...</Text>
            </View>
          }
          { this.props.noneResult &&  
            <Text className="empty-tip">{ this.props.noneResultTips }</Text>
          }
        </View>
   
      </View >)
  }
}
export default ApplyList as ComponentClass<PageOwnProps, PageState>

