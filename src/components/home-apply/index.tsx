import { ComponentClass } from 'react'
import Taro, { Component } from '@tarojs/taro'
import { View, Text, Image, Input } from '@tarojs/components'
import { connect } from '@tarojs/redux'

import inputLeft from '../../asset/images/home-apply-box/input_left@2x.png'
import inputRight from '../../asset/images/home-apply-box/input_right@2x.png'
import buttonImg from '../../asset/images/apply_button@2x.png'

import ImageButton from '../img-button'
import { default as ProductUtils } from '../../utils/product'
import { User as UserUtils } from '../../utils/user'

import './index.scss'

const productUtils = new ProductUtils()
const userUtils = new UserUtils()

type PageStateProps = {
  isLoginAlready: boolean,
  isLoginValid: boolean,
}

type PageDispatchProps = {}

type PageOwnProps = {
  depositMoney: number,   // 默认的申请金额
}

type PageState = {
  depositInput: number,   // 输入的贷款金额
}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps

interface HomeApply {
  props: IProps;
  state: PageState;
}

/**
 * 首页立即申请组件
 */
@connect(({common}) => ({
  ...common
}), (dispatch) => ({}))
class HomeApply extends Component {

  constructor(props) {
    super(props);
    this.state = { depositInput: NaN };  // 输入贷款额度初始化
  }

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }

  render() {
    return (
      <View className='home-apply-container'>
        <View className="input-intro">
            <Image className="decorate-image" src={inputLeft} />
            <Text className="decorate-text">输入金额，立马到手</Text>
            <Image className="decorate-image" src={inputRight} />
        </View>
        <View className="input-box">
            <Input 
              type="number" 
              value={ this.state.depositInput + '' } 
              onInput={this.OnDepositInput} 
              placeholder={ productUtils.toThousand(this.props.depositMoney) }
              className="money-input"/>
        </View>
        <View className="button-box" onClick={ this._onApply.bind(this) }>
            <ImageButton imgSrc={buttonImg} buttonText={'立即申请'}></ImageButton>
        </View>
      </View>
    )
  }

  /**
   * 贷款金额输入的处理方法
   * @param e 
   */
  OnDepositInput = (e) => {
    // console.log('OnDepositInput e = ', e)
    this.setState({
      depositInput: e.detail.value,
    })
  }

  /**
   * 点击立即申请按钮的处理方法
   * @param e 
   */
  _onApply(e) {
    const url = '/pages/stepApplyOne/index'
    // const { isLoginValid } = this.props
    // console.log('_onApply isLoginValid = ', isLoginValid)
    // if (isLoginValid) {
    //   Taro.navigateTo({
    //     url: '/pages/stepApplyOne/index'
    //   })
    // } else {
    //   userUtils.checkIsLogin(url)
    // }
    Taro.navigateTo({
      url: `${url}?deposit=${this.state.depositInput || this.props.depositMoney}`
    })
  }
}

export default HomeApply as ComponentClass<PageOwnProps, PageState>
