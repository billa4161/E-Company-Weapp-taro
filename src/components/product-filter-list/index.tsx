import { ComponentClass } from 'react'
import Taro, { Component } from '@tarojs/taro'
import { View, Text, Image, Button } from '@tarojs/components'
import { connect } from '@tarojs/redux'

import InputOption from '../../model/product-list-input-option';

import ProductFilter from '../product-filter';

import './index.scss'

type PageStateProps = {}

type PageDispatchProps = {}

type PageOwnProps = {
  // 输入的选项，作为默认选中选项的参数
  inputOption: InputOption,
}

type PageState = {

}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps

interface ProductFilterList {
  props: IProps;
  state: PageState;
}

/**
 * 带过滤功能的产品列表组件
 */
@connect(({ }) => ({}), (dispatch) => ({}))
class ProductFilterList extends Component {

  constructor(props) {
    super(props);
  }

  componentWillReceiveProps() {

  }

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }

  render() {
    return (
      <View className="product-filter-list-container">
        <View className="filter-selection-box">
          <ProductFilter inputOption={this.props.inputOption}/>
        </View>
      </View >)
  }
}

export default ProductFilterList as ComponentClass<PageOwnProps, PageState>
