import { ComponentClass } from 'react'
import Taro, { Component } from '@tarojs/taro'
import { View, Text, } from '@tarojs/components'
import { connect } from '@tarojs/redux'

import { default as ProductUtils } from '../../utils/product'

import './index.scss'

const productUtils = new ProductUtils()

type PageStateProps = {}

type PageDispatchProps = {}

type PageOwnProps = {
  product: any,   // 产品对象
  deposit: number,  // 贷款额度
  timeLimit: number,   // 贷款期限
}

type PageState = {
  isOpen: boolean,  // 是否展开
}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps

interface BankDetailHeader {
  props: IProps;
  state: PageState;
}

/**
 * 首页立即申请组件
 */
@connect(({ }) => ({}), (dispatch) => ({}))
class BankDetailHeader extends Component {

  constructor(props) {
    super(props);

    this.state = {
      isOpen: false
    }
  }

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }

  render() {
    const { product, deposit, timeLimit } = this.props
    const { isOpen } = this.state

    return (
      <View className="detail-table-box-container">
        <View className="table-box">

          <View className="row">
            <Text className="title">详情说明</Text>
            <View className="triangle"></View>
          </View>

          <View className="row">
            <View className="label">
              <Text className="">贷款额度/期限</Text>
            </View>
            <View className="content">
              <Text className="content">{productUtils.converToTenThousand(deposit)}万/{timeLimit}个月</Text>
            </View>
          </View>

          <View className="row">
            <View className="label">
              <Text className="">利息</Text>
            </View>
            <View className="content">
              <Text className="content">
                {
                  productUtils.convertToInterestStr(deposit, timeLimit, product)
                }
                {
                  product.combination_rate_type === 1 ? `(${productUtils.converCombinationRateTypeToDisplayStr(product.combination_rate_type, product.combination_rate)}/每月)` : null
                }
              </Text>
            </View>
          </View>

          <View className="row">
            <View className="label">
              <Text className="">一次性费用</Text>
            </View>
            <View className="content">
              <Text className="content">{productUtils.convertToNonrecurringExpenseStr(deposit, product)}</Text>
            </View>
          </View>
          <View className="row">
            <View className="label">
              <Text className="">担保金</Text>
            </View>
            <View className="content">
              <Text className="content">
                {productUtils.convertToGuaranteeMoneyStr(deposit, product)}
                {product.disposable_cost_type === 1 ? `(${product.guarantee}%)` : ''}
              </Text>
            </View>
          </View>

        </View>

        <View className={`interest-detail ${isOpen ? '' : 'close'}`}>
          <View className="title text">
            <Text>利率说明</Text>
          </View>

          <View className="table-box">
            <View className="row bg-white">
              <View className="label">
                <Text className="">贷款期限 (月)</Text>
              </View>
              <View className="content">
                <Text className="content">{product.min_time_limit}-{product.max_time_limit}个月</Text>
              </View>
            </View>
            <View className="row">
              <View className="label">
                <Text className="">利息</Text>
              </View>
              <View className="content">
                <Text className="content">
                  {productUtils.converCombinationRateTypeToDisplayStr(product.combination_rate_type, product.combination_rate)}
                  {product.combination_rate_type === 1 ? '/每月' : ''}
                </Text>
              </View>
            </View>
          </View>
        </View>

        <View className="expand-box" onClick={this.onOpenInterestDetail}>
          <Text className="text">{isOpen ? '收起' : '查看利率详情'}</Text>
          <Text className={`iconfont icon-f11 ${!isOpen ? 'down' : ''}`}></Text>
        </View>
      </View>)
  }

  /**
   * 点击查看利率详情的处理方法
   */
  onOpenInterestDetail = () => {
    const status = !this.state.isOpen
    this.setState({
      isOpen: status
    })
  }
}

export default BankDetailHeader as ComponentClass<PageOwnProps, PageState>
