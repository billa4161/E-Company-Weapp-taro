import { ComponentClass } from 'react'
import Taro, { Component } from '@tarojs/taro'
import { View, Text, Image } from '@tarojs/components'
import { connect } from '@tarojs/redux'

import { default as ProductUtils } from '../../utils/product'

import './index.scss'
import flow1Img from '../../asset/images/bank-apply-flow/flow1.png'
import flow2Img from '../../asset/images/bank-apply-flow/flow2.png'
import flow3Img from '../../asset/images/bank-apply-flow/flow3.png'
import flow4Img from '../../asset/images/bank-apply-flow/flow4.png'
import flowArrowImg from '../../asset/images/bank-apply-flow/flow-arrow.png'

const productUtils = new ProductUtils()

type PageStateProps = {}

type PageDispatchProps = {}

type PageOwnProps = {}

type PageState = {}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps

interface BankApplyFlow {
  props: IProps;
  state: PageState;
}

/**
 * 银行产品申请流程组件
 */
@connect(({ }) => ({}), (dispatch) => ({}))
class BankApplyFlow extends Component {

  constructor(props) {
    super(props);

    this.state = {
      isOpen: false
    }
  }

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }

  render() {

    return (<View className="bank-apply-flow-container">
      <View className="flow-title">
        <Text className="text">办理流程</Text>
      </View>
      <View className="flow-container">
        <View className="item">
          <Image className="image" src={flow1Img}></Image>
          <Text className="text">申请贷款</Text>
        </View>
        <View className="item">
          <Image className="arrow" src={flowArrowImg}></Image>
        </View>
        <View className="item">
          <Image className="image" src={flow2Img}></Image>
          <Text className="text">客服跟进</Text>
        </View>
        <View className="item">
          <Image className="arrow" src={flowArrowImg}></Image>
        </View>
        <View className="item">
          <Image className="image" src={flow3Img}></Image>
          <Text className="text">门店办理</Text>
        </View>
        <View className="item">
          <Image className="arrow" src={flowArrowImg}></Image>
        </View>
        <View className="item">
          <Image className="image" src={flow4Img}></Image>
          <Text className="text">审批放款</Text>
        </View>
      </View>
    </View>)
  }
}

export default BankApplyFlow as ComponentClass<PageOwnProps, PageState>
