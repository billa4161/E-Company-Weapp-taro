import { ComponentClass } from 'react'
import Taro, { Component } from '@tarojs/taro'
import { View, Text, Image, Button } from '@tarojs/components'

import Apply from '../../model/apply'
import { Utils } from '../../utils/utils'
import ProductUtils from '../../utils/product'
import Constants from '../../config/constants'

import './index.scss'

type PageStateProps = {}

type PageDispatchProps = {}

type PageOwnProps = {
  apply: Apply,   // 申请对象
}

type PageState = {

}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps

interface ApplyListItem {
  props: IProps;
  state: PageState;
}

const utils = new Utils()
const productUtils = new ProductUtils()

/**
 * 产品列表的元素组件
 */
class ApplyListItem extends Component {

  constructor(props) {
    super(props);
  }

  componentWillReceiveProps(nextProps) { }

  componentWillMount() { }

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }

  render() {
    const { apply } = this.props

    return (<View className="apply-list-item-container">
      <View className="row">
        <Text className="title"> {apply.product_title} </Text>
        <Text className="date">{utils.convertOrderNumToDate(apply.order_number + '')}</Text>
      </View>

      <View className="row">
        <View className="row-item">
          <View className="view">
            <Text className="label text">申请金额</Text>
            <Text className="content text">{apply.loan_limit}元</Text>
          </View>
          <View className="view">
            <Text className="label text">申请期限</Text>
            <Text className="content text">{apply.loan_date_limit}月</Text>
          </View>
        </View>

        <View className="row-item">
          <View className="view">
            <Text className="label text">订单号</Text>
            <Text className="content text">{apply.order_number}</Text>
          </View>
          <View className="view">
            <Text className="label text">产品类型</Text>
            <Text className="content text">{productUtils.convertProductTypeToString(apply.loan_type)}</Text>
          </View>
        </View>
      </View>
    </View>)
  }
}

export default ApplyListItem as ComponentClass<PageOwnProps, PageState>

