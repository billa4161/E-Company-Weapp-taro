import { ComponentClass } from 'react'
import Taro, { Component, Events } from '@tarojs/taro'
import { View, Text, Image, Button } from '@tarojs/components'
import { connect } from '@tarojs/redux'

import SelectOption from '../../model/select-option';
import Constants from '../../config/constants';

import './index.scss'

type PageStateProps = {}

type PageDispatchProps = {}

type PageOwnProps = {
  // 所有选项
  allOptions: Array<SelectOption>,
  // 菜单描述
  menuDescription?: string,
  // 选中的选项的值
  selectedOption: SelectOption,
  // 选择选项的回调方法
  optionSelectCallback?: Function,
  // 组件的外部输入样式对象
  styleObj?: any,
  // 菜单的类别  代表菜单的类别 例如：'deposit', 'time', 'sort'
  type?: string,
  // 事件的后缀
  eventSuffix?: string,
}

type PageState = {
  // 选中的选项
  selfSelectedOption: SelectOption,
}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps

interface ProductFilterMenu {
  props: IProps;
  state: PageState;
}

// const events = new Events()

/**
 * 过滤组件的菜单子组件
 */
class ProductFilterMenu extends Component {

  constructor(props) {
    super(props);

    this.state = {
      selfSelectedOption: this.props.selectedOption,
    };
  }

  public static defaultProps: PageOwnProps = {
    allOptions: [],
    menuDescription: '',
    selectedOption: {
      name: '',
      value: '',
    },
    optionSelectCallback: () => {},
  };

  componentWillReceiveProps(nextProps) {
    // 根据输入 selectedOption 更新 state
    const { selectedOption } = nextProps
    if (selectedOption) {
      this.setState({
        selfSelectedOption: selectedOption,
      })
    }
  }

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }

  render() {
    const selfSectctedValue = this.state.selfSelectedOption.value;
    const menus = this.props.allOptions.map(item => {
      return <View className="option-item view" key={item.value} onClick={this.optionTap.bind(this)} data-option={JSON.stringify(item)}>
        <Text className={`text ${item.value === selfSectctedValue ? 'active' : ''}`}>{item.name}</Text>
        {item.value === selfSectctedValue && <Text className="iconfont icon-gou9 active"></Text>}
      </View>
    });
    return (
      <View className="product-filter-menu-container" style={this.props.styleObj}>
        <View className="menu-box">
          {
            this.props.menuDescription &&
            <View className="menu-description view">
              <Text className="text">{this.props.menuDescription}</Text>
            </View>
          }
          { menus }
        </View>
      </View>)
  }

  /**
   * 选项点击处理方法
   * @param {*} e 
   */
  optionTap(e) {
    // console.log('optionTap e = ', e)
    const option = e.currentTarget.dataset.option;
    const optionObj = JSON.parse(option);
    // 更新选中 option 对象
    this.setState({
      selfSelectedOption: optionObj,
    });

    // 发射自定义事件
    Taro.eventCenter.trigger(`${Constants.FILTER_MENU_SELECT_EVENT}${this.props.eventSuffix}`, { option: optionObj, type: this.props.type, });
  }
}

export default ProductFilterMenu as ComponentClass<PageOwnProps, PageState>
