import { ComponentClass } from 'react'
import Taro, { Component } from '@tarojs/taro'
import { View, Text, Image, Button, Picker } from '@tarojs/components'
import { connect } from '@tarojs/redux'

import { default as ProductUtils } from '../../utils/product'
import { User as UserUtils } from '../../utils/user'
import { Utils } from '../../utils/utils'
import Constants from '../../config/constants'

import bgTopImg from '../../asset/images/fast-apply-header/header-bg.jpg'
import rowDividerImg from '../../asset/images/fast-apply-header/row-divider.png'
import columnDividerImg from '../../asset/images/fast-apply-header/column-divider.png'
import './index.scss'

const productUtils = new ProductUtils()
const userUtils = new UserUtils()
const utils = new Utils()

type PageStateProps = {}

type PageDispatchProps = {}

type PageOwnProps = {
  product: any,   // 产品对象
  form: any,      // 表单对象
  allTimeLimitOptions: any[],  // 贷款期限选项
  eventRandomSuffix: string,   // 事件后缀字符串
}

type PageState = {
  // 是否展开详情
  isOpen: boolean,
}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps

interface FastProductApplyHeader {
  props: IProps;
  state: PageState;
}

/**
 * 快速产品申请页面的头组件
 */
@connect(({}) => ({}), (dispatch) => ({}))
class FastProductApplyHeader extends Component {

  constructor(props) {
    super(props);

    this.state = {
      isOpen: false,
    }
  }

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }

  render() {
    const { product, form, allTimeLimitOptions } = this.props
    const { isOpen } = this.state

    let conditionRows
    const conditionText = product.apply_for_position
    if (conditionText && conditionText.length > 0) {
      const rowArr = conditionText.split('\n')
      conditionRows = rowArr.map((item, index) => {
        return (<Text key={index} className="text" decode={false}>
          {item}
        </Text>)
      })
    }

    return (<View className="fast-product-detail-header-container">
      {/* <!-- 产品头部box --> */}
      <View className="header">
        <Image className="bg-img" src={bgTopImg}></Image>
        <View className="content-box">
          {/* <!-- 产品标题 --> */}
          <View className="title-box">
            <Image className="image" src={product.logo}></Image>
            <Text className="text">{product.title}</Text>
          </View>
          {/* <!-- 填写额度，期限 --> */}
          <View className="edit-box">
            <View className="item" onClick={this.onDepositTap}>
              <Text className="top">额度：{product.min_deposit}-{product.max_deposit}</Text>
              <View className="bottom">
                <Text className="text">{form.loan_limit}</Text>
                <Text className="text">元</Text>
                <Text className="iconfont icon-dkw_tianxie"></Text>
              </View>
            </View>
            <View className="item">
              <Text className="top require">贷款期限</Text>
              <View className="bottom">
                <Picker className="selector" onChange={this.onTimeLimitPickerChange} value={utils.getOptionIndex(form.loan_date_limit, allTimeLimitOptions)} range={allTimeLimitOptions}>
                  <Text className="text">{form.loan_date_limit}</Text>
                  <Text className="text time-unit">月</Text>
                  <Text className="iconfont icon-down-arrow"></Text>
                </Picker>
              </View>
            </View>
          </View>
          {/* <!-- 行分割线 --> */}
          <View className="row-divider">
            <Image className="image" src={rowDividerImg}></Image>
          </View>
          {/* <!-- 还款金额 利息和费用 到账金额 --> */}
          <View className="fee-box">
            <View className="item">
              <Text className="info">还款金额</Text>
              <Text className="number">{ productUtils.converMoneyToDisplay(productUtils.caculateRepaymentAndInterestAndFee(form.loan_limit, form.loan_date_limit, product).repayment) } </Text>
            </View>
            <View className="cloum-divider">
              <Image src={columnDividerImg}></Image>
            </View>
            <View className="item">
              <Text className="info">利息和费用</Text>
              <Text className="number">{ productUtils.converMoneyToDisplay(productUtils.caculateRepaymentAndInterestAndFee(form.loan_limit, form.loan_date_limit, product).interestAndFee) }</Text>
            </View>
            <View className="cloum-divider">
              <Image src={columnDividerImg}></Image>
            </View>
            <View className="item">
              <Text className="info">到账金额</Text>
              <Text className="number">{ productUtils.converMoneyToDisplay(productUtils.caculateGetMoney(form.loan_limit, form.loan_date_limit, product)) }</Text>
            </View>
          </View>
        </View>
      </View>
      {/* <!-- 产品详情分块 --> */}
      <View className="product-detail-box">
        <View className="top-box">
          <View className="view">
            <Text className="iconfont icon-web-icon-"></Text>
            <Text className="text">{productUtils.converLoanTimeToDisplayStr(product.loan_time, product.loan_time_type)}</Text>
          </View>
          <View className="view">
            <Text className="iconfont icon-qian"></Text>
            <Text className="text">月费率: {productUtils.converCombinationRateTypeToDisplayStr(product.combination_rate_type, product.combination_rate)}</Text>
          </View>
        </View>
        <View className={`middle-box ${ isOpen ? '' : 'close' }`}>
          <View>
            <Text className="title">申请条件</Text>
          </View>
          <Text className="content">
            {conditionRows}
          </Text>
        </View>
        <View className="bottom-box" onClick={this.onOpenProductDetail}>
          <Text className="text">{isOpen ? '收起' : '查看产品详情'}</Text>
          <Text className={`iconfont icon-f11 ${!isOpen ? 'down' : ''}`}></Text>
        </View>
      </View>
    </View >
    )
  }

  /**
   * 点击编辑额度的处理方法
   * @param {*} e 
   */
  onDepositTap = (e) => {
    console.log('onDepositTap !! e = ', e)
    // this.setState({
    //   isShowDeposit: true
    // })
  }

  /**
   * 贷款期限改变的处理方法
   */
  onTimeLimitPickerChange = (e) => {
    // console.log('onTimeLimitPickerChange  e = ', e)

    const { value } = e.detail
    const optionValue = utils.getOptionValueByIndex(value, this.props.allTimeLimitOptions)

    // 发射期限改变事件
    Taro.eventCenter.trigger(`${Constants.FAST_PRODUCT_APPLY_TIME_CHANGE_EVENT}${this.props.eventRandomSuffix}`, {
      loan_date_limit: optionValue,
    })
  }

  /**
   * 点击查看产品详情的处理方法
   */
  onOpenProductDetail = () => {
    this.setState({
      isOpen: !this.state.isOpen
    })
  }

}

export default FastProductApplyHeader as ComponentClass<PageOwnProps, PageState>
