import { ComponentClass } from 'react'
import Taro, { Component } from '@tarojs/taro'
import { View, Text, Image, Button } from '@tarojs/components'
import { connect } from '@tarojs/redux'

import bgTopImg from '../../asset/images/bank-detail-header/bg_top.png'
import bgMiddleImg from '../../asset/images/bank-detail-header/bg_middle.png'
import bgBottomImg from '../../asset/images/bank-detail-header/bg_bottom.png'
import ProductTag from '../product-tag'

import { default as ProductUtils } from '../../utils/product'
import { User as UserUtils } from '../../utils/user'

import './index.scss'

const productUtils = new ProductUtils()
const userUtils = new UserUtils()

type PageStateProps = {}

type PageDispatchProps = {}

type PageOwnProps = {
  product: any,   // 产品对象
}

type PageState = {
  // 产品类型tag
  productTypeTagStyle: any,
  // 易通过tag
  easyTagStyle: any,
}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps

interface BankDetailHeader {
  props: IProps;
  state: PageState;
}

/**
 * 首页立即申请组件
 */
@connect(({ }) => ({}), (dispatch) => ({}))
class BankDetailHeader extends Component {

  constructor(props) {
    super(props);

    this.state = {
      productTypeTagStyle: {
        border: '1px solid #23aab6',
        color: '#23aab6',
        backgroundColor: '#fff',
        fontSize: '10px',
        lineHeight: '1',
      },
      easyTagStyle: {
        border: '1px solid #f9a347',
        color: '#f9a347',
        backgroundColor: '#fff',
        fontSize: '10px',
        lineHeight: '1',
      },
    }
  }

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }

  render() {
    const { product } = this.props

    return (<View className="bank-product-detail-header-container">
      <View className="top"></View>
      <View className="bottom"></View>
      <View className="decorate decorate-top">
        <Image src={bgTopImg}></Image>
      </View>
      <View className="decorate decorate-middle">
        <Image src={bgMiddleImg}></Image>
      </View>
      <View className="decorate decorate-bottom">
        <Image src={bgBottomImg}></Image>
        <View className="content-box">
          <View className="left">
            <Image src={product.logo}></Image>
          </View>
          <View className="right">
            <Text className="title">{product.title}</Text>
            <View className="info">
              <Text className="text">放款时间:{productUtils.converLoanTimeToDisplayStr(product.loan_time, product.loan_time_type)}</Text>
              {
                (product.apply_count > 0 && product.apply_count < 10) ? (<Text className="text">申请人数:少于10人</Text>) : null
              }
              {
                (product.apply_count >= 10 && product.apply_count < 1000) ? <Text className="text">申请人数:{product.apply_count}</Text> : null
              }
              {
                product.apply_count > 1000 ? (<Text className="text">申请人数:1000+</Text>) : null
              }
              {
                !product.apply_count ? (<Text className="text">申请人数:</Text>) : null
              }
            </View>
            <View className="tag-box">
              {
                product.product_type > 0 ? <ProductTag text={productUtils.convertProductTypeToString(this.props.product.product_type)} styleClass={this.state.productTypeTagStyle} /> : null
              }
              {
                product.easy_pass === 1 ? <ProductTag text="易通过" styleClass={this.state.easyTagStyle} /> : null
              }
            </View>
          </View >
        </View >
      </View >
    </View >
    )
  }
}

export default BankDetailHeader as ComponentClass<PageOwnProps, PageState>
