import { ComponentClass } from 'react'
import Taro, { Component, Config } from '@tarojs/taro'
import { View, Swiper, SwiperItem, Image } from '@tarojs/components'
import { connect } from '@tarojs/redux'

import { updateBanner } from '../../actions/public-data'
import { noConsole } from '../../config'

import './index.scss'

// #region 书写注意
// 
// 目前 typescript 版本还无法在装饰器模式下将 Props 注入到 Taro.Component 中的 props 属性
// 需要显示声明 connect 的参数类型并通过 interface 的方式指定 Taro.Component 子类的 props
// 这样才能完成类型检查和 IDE 的自动提示
// 使用函数模式则无此限制
// ref: https://github.com/DefinitelyTyped/DefinitelyTyped/issues/20796
//
// #endregion

type PageStateProps = {
  // swiper的banner图片src数组
  bannerSrcs: string[]
}

type PageDispatchProps = {
  updateBanner: () => void
}

type PageOwnProps = {}

type PageState = {}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps

interface HomeSwiper {
  props: IProps;
}

/**
 * 首页banner组件
 */
@connect(({ home }) => ({
  bannerSrcs: home['bannerSrcs'],
}), (dispatch) => ({
  updateBanner() {
    dispatch(updateBanner())
  },
}))
class HomeSwiper extends Component {

  componentWillReceiveProps(nextProps) {
    if (!noConsole) {
      // console.log('home-header component, componentWillReceiveProps', this.props, nextProps)
    }
  }

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }

  render() {
    const swiperItem = this.props.bannerSrcs.map((item, index) => {
      return <SwiperItem key={index}>
        <Image src={item} className="slide-image" style="width: 100%; height: 100%;" />
      </SwiperItem>
    })
    return (
      <View className='home-swiper-container'>
        <Swiper className="swiper-box" indicatorDots autoplay interval={3000} circular indicatorColor="rgba(255,255,255,.9)" indicatorActiveColor="#4e8df7">
          { swiperItem }
        </Swiper>
      </View>
    )
  }

  // 搜索框点击的处理方法
  onSearching() {
    Taro.showToast({
      title: '暂无功能',
      icon: 'none',
    })
  }
}

// #region 导出注意
//
// 经过上面的声明后需要将导出的 Taro.Component 子类修改为子类本身的 props 属性
// 这样在使用这个子类时 Ts 才不会提示缺少 JSX 类型参数错误
//
// #endregion

export default HomeSwiper as ComponentClass<PageOwnProps, PageState>
