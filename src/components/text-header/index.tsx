import { ComponentClass } from 'react'
import Taro, { Component, Config } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'
import { connect } from '@tarojs/redux'

import { changeMyLocation } from '../../actions/location'
import { noConsole } from '../../config'

import './index.scss'

// #region 书写注意
// 
// 目前 typescript 版本还无法在装饰器模式下将 Props 注入到 Taro.Component 中的 props 属性
// 需要显示声明 connect 的参数类型并通过 interface 的方式指定 Taro.Component 子类的 props
// 这样才能完成类型检查和 IDE 的自动提示
// 使用函数模式则无此限制
// ref: https://github.com/DefinitelyTyped/DefinitelyTyped/issues/20796
//
// #endregion

type PageStateProps = {
  // 用户当前位置信息
  userLocation: {
    city: string; // 用户当前所在的城市名 例如： '全国' / '北京'
  }
}

type PageDispatchProps = {
  changeMyLocation: () => void
}

type PageOwnProps = {
  title: string,    // 头部的标题
  headerStyle?: any, // 样式
}

type PageState = {}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps

interface TextHeader {
  props: IProps;
}

/**
 * h5 页面的头部标题组件
 */
@connect(({}) => ({}), () => ({}))
class TextHeader extends Component {

  constructor(props) {
    super(props)
  }

  public static defaultProps: PageOwnProps = {
   title: '',
   headerStyle: {
     fontSize: '16px',
     letterSpacing: '2px',
     textAlign: 'center',
     color: '#fff',
     backgroundColor: '#4e8df7',
     padding: '10px 0',
   }
  };

  componentWillReceiveProps(nextProps) {
    if (!noConsole) {
      // console.log('home-header component, componentWillReceiveProps', this.props, nextProps)
    }
  }

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }

  render() {
    const { title, headerStyle } = this.props

    return (
      <View className='text-header-container' style={ headerStyle }>
        <Text>{title}</Text>
      </View>
    )
  }
}

// #region 导出注意
//
// 经过上面的声明后需要将导出的 Taro.Component 子类修改为子类本身的 props 属性
// 这样在使用这个子类时 Ts 才不会提示缺少 JSX 类型参数错误
//
// #endregion

export default TextHeader as ComponentClass<PageOwnProps, PageState>
