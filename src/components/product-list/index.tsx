import { ComponentClass } from 'react'
import Taro, { Component } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'

import ProductType from '../../model/product'
import Apply from '../../model/apply'
import ProductListItem  from '../product-list-item'
import Loading from '../loading'

import './index.scss'

type PageStateProps = {
  
}

type PageDispatchProps = {}

type PageOwnProps = {
  // 产品列表
  dataArray: ProductType[],
  // 是否要显示勾选框
  isShowCheck: boolean,
  // 是否要显示放款时间
  isShowLoanTime: boolean,
  // 是否要显示申请按钮
  isShowApply: boolean,
  // 是否要显示取消收藏
  isShowCollectCancel: boolean,
  // 加载中
  loading: boolean,
  // 没有加载到数据的标志位
  noneResult: boolean,
  // 没有加载到数据的提示语
  noneResultTips: string,
  // 用户申请数据
  userApplyList: Apply[],
}

type PageState = {
  
}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps

interface ProductList {
  props: IProps;
  state: PageState;
}

/**
 * 产品列表组件
 * 只有展示数据的功能
 */
class ProductList extends Component {

  constructor(props) {
    super(props);

    this.state = {
      
    }
  }

  componentWillReceiveProps(nextProps) {

  }

  componentWillUnmount() {

  }

  componentDidShow() { }

  componentDidHide() { }

  render() {
    const { dataArray } = this.props

    let productItems: any[] = []
    if (dataArray) {
      productItems = dataArray.map((item) => {
        return <View key={item.product_id} className='product-item'>
          <ProductListItem 
            product={item}
            isShowApply={this.props.isShowApply}
            isShowCheck={this.props.isShowCheck}
            isShowCollectCancel={this.props.isShowCollectCancel}
            isShowLoanTime={this.props.isShowLoanTime}
            userApplyList={this.props.userApplyList} />
        </View>
      }) 
    }
    return (
      <View className="product-list-container">
        <View className="products-container">
          {productItems}
        </View>
        <View className='bottom-box'>
          { 
            this.props.loading && 
            <View className="loading">
                <Loading />
                <Text className='text'>加载中...</Text>
            </View>
          }
          { this.props.noneResult &&  
            <Text className="empty-tip">{ this.props.noneResultTips }</Text>
          }
        </View>
   
      </View >)
  }
}
export default ProductList as ComponentClass<PageOwnProps, PageState>

