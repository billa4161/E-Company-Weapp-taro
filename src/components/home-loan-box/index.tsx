import { ComponentClass } from 'react'
import Taro, { Component, Config } from '@tarojs/taro'
import { View, Text, Image } from '@tarojs/components'
import { connect } from '@tarojs/redux'

import applyIcon from '../../asset/images/loan-icons/apply_icon@2x.png'; 
import bankIcon from '../../asset/images/loan-icons/bank_loan_icon@2x.png'; 
import fastIcon from '../../asset/images/loan-icons/fast_loan_icon@2x.png'; 

import './index.scss'

type PageStateProps = { }

type PageDispatchProps = {
  dispatchActiveTab: (activeTabIndex: number) => void,
}

type PageOwnProps = {}

type PageState = {}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps

interface HomeLoan {
  props: IProps;
  state: PageState;
}

/**
 * 首页贷款产品跳转组件
 */
@connect(({ common, loading }) => ({
  ...common,
  ...loading,
}), (dispatch) => ({
  dispatchActiveTab(activeTabIndex: number) {
    dispatch({
      type: 'common/save',
      payload: {
        activeTabIndex,
      }
    })
  },
}))
class HomeLoan extends Component {

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }

  render() {
    return (
      <View className='home-loan-container'>
        <View className="loan-item" onClick={this._goToShop.bind(this, 'fast')} data-loantype="fast">
            <Image className="image" src={fastIcon}></Image>
            <Text className="text">极速贷款</Text>
        </View>
        <View className="loan-item" onClick={this._goToShop.bind(this, 'bank')} data-loantype="bank">
            <Image className="image" src={bankIcon}></Image>
            <Text className="text">银行贷款</Text>
        </View>
        <View className="loan-item" onClick={this._goToMyApply}>
            <Image className="image" src={applyIcon}></Image>
            <Text className="text">我的申请</Text>
        </View>
      </View>
    )
  }

  /**
   * 跳转到商城页
   * @param loantype 产品类型 fast 或 bank
   * @param e 
   */
  _goToShop(loantype: string, e) {
    // console.log('_goToShop e = ', e)

    // 更新common state
    let index = loantype === 'fast' ? 0 : 1
    this.props.dispatchActiveTab(index)

    if (process.env.TARO_ENV === 'weapp') {
      Taro.switchTab({
        url: 'pages/shop/index',
      })
    } else {
      // 要用 redirectTo, 否则打开页面的组件会有奇怪问题，例如获取不了高度
      Taro.redirectTo({
        url: '/pages/shop/index'
      })
    }
  }

  /**
   * 跳转到我的申请页
   * @param e 
   */
  _goToMyApply(e) {
    // console.log('_goToMyApply e = ', e)

    Taro.navigateTo({
      url: '/pages/myApply/index'
    })
  }
}

// #region 导出注意
//
// 经过上面的声明后需要将导出的 Taro.Component 子类修改为子类本身的 props 属性
// 这样在使用这个子类时 Ts 才不会提示缺少 JSX 类型参数错误
//
// #endregion

export default HomeLoan as ComponentClass as ComponentClass<PageOwnProps, PageState>
