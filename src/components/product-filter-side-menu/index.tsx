import { ComponentClass } from 'react'
import Taro, { Component } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'

import SelectOption from '../../model/select-option';
import Constants from '../../config/constants';
import { productTypeOptions, mortgageTypeOptions, jobsOptions } from '../../config';

import './index.scss'

type PageStateProps = {}

type PageDispatchProps = {}

type PageOwnProps = {
  // 选中的产品类型,默认选中第一个
  selectedProductType: SelectOption,
  // 选中的职业身份,默认选中第一个
  selectedjob: SelectOption,
  // 选中的抵押类型，默认选中第一个
  selectedMrotageType: SelectOption,
  // 是否显示组件
  isShow: boolean,
  // 时间后缀
  eventSuffix?: string,
}

type PageState = {
  // 产品选项菜单选项
  productTypeOptions: SelectOption[],
  // 职业身份选项菜单选项
  jobOptions: SelectOption[],
  // 抵押类型选项
  mortgageTypeOptions: SelectOption[],
  // 已选中的产品选项
  selfSelectedProductType: SelectOption,
  // 已选中的职业身份
  selfSelectedJob: SelectOption,
  // 已选中的抵押类型
  slefSelectedMortgageType: SelectOption,
}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps

interface ProductFilterSideMenu {
  props: IProps;
  state: PageState;
}

/**
 * 过滤组件的菜单子组件
 */
class ProductFilterSideMenu extends Component {

  constructor(props) {
    super(props);

    this.state = {
      productTypeOptions,
      jobOptions: jobsOptions,
      mortgageTypeOptions,
      selfSelectedProductType: props.selectedProductType,
      selfSelectedJob: props.selectedjob,
      slefSelectedMortgageType: props.selectedMrotageType,
    };
  }

  public static defaultProps: PageOwnProps = {
    selectedProductType: productTypeOptions[0],
    selectedjob: jobsOptions[0],
    selectedMrotageType: mortgageTypeOptions[0],
    isShow: false,
  }

  componentWillReceiveProps(nextProps) {
    
    // 根据输入更新 state
    const { selectedProductType, selectedjob, selectedMrotageType } = nextProps

    if (selectedProductType) {
      this.setState({
        selfSelectedProductType: selectedProductType,
      })
    }
    if (selectedjob) {
      this.setState({
        selfSelectedJob: selectedjob,
      })
    }
    if (selectedMrotageType) {
      this.setState({
        slefSelectedMortgageType: selectedMrotageType,
      })
    }
    
  }

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }

  render() {
    const productTypes = this.state.productTypeOptions.map((item) => {
      const selfSelectedProductTypeVal = this.state.selfSelectedProductType.value;
      // 需要使用 process.env.TARO_ENV === 'weapp' 做兼容
      return <View 
        key={item.value}
        onClick={ process.env.TARO_ENV === 'weapp' ?  this.onProductTypeOptionTap : this.onProductTypeOptionTap.bind(this)}
        data-producttype={JSON.stringify(item)}
        className={`item ${item.value === selfSelectedProductTypeVal ? 'active' : ''}`}>
        <Text>{item.name}</Text>
      </View>
    });
    const jobs = this.state.jobOptions.map((item) => {
      const selfSelectedJobVal = this.state.selfSelectedJob.value;
      return <View 
        key={item.value} 
        onClick={ process.env.TARO_ENV === 'weapp' ?  this.onJobOptionTap : this.onJobOptionTap.bind(this)} 
        className={`item ${item.value === selfSelectedJobVal ? 'active' : ''}`} 
        data-job={JSON.stringify(item)}>
        <Text>{item.name}</Text>
      </View>
    });
    const mortgageTypes = this.state.mortgageTypeOptions.map((item) => {
      const slefMortgageTypeVal = this.state.slefSelectedMortgageType.value;
      return <View 
        key={item.value} 
        onClick={ process.env.TARO_ENV === 'weapp' ?  this.onMortgageTypeOptionTap : this.onMortgageTypeOptionTap.bind(this)} 
        className={`item ${item.value === slefMortgageTypeVal ? 'active' : ''}`} 
        data-mortage={JSON.stringify(item)}>
        <Text>{item.name}</Text>
      </View>
    });
    return (
      <View className={`product-filter-side-menu-container ${this.props.isShow ? '' : 'side-menu-hide'}`}>
        <View className="blank" onClick={ this.onClose.bind(this) }></View>
        <View className="content">
          <View className="row-select">
            <View className="label">
              <Text>产品种类</Text>
            </View>
            <View className="select-box">
              {productTypes}
            </View>
          </View>

          <View className="row-select">
            <View className="label">
              <Text>职业身份</Text>
            </View>
            <View className="select-box">
              {jobs}
            </View>
          </View>

          <View className="row-select">
            <View className="label">
              <Text>抵押类型</Text>
            </View>
            <View className="select-box">
              {mortgageTypes}
            </View>
          </View>

          <View className="button-box">
            <View className="function-button" onClick={ process.env.TARO_ENV === 'weapp' ?  this.onReset : this.onReset.bind(this)}>
              <button><Text>重置</Text></button>
            </View>
            <View className="function-button submit" onClick={ process.env.TARO_ENV === 'weapp' ?  this.onSubmit : this.onSubmit.bind(this)}>
              <button><Text>完成</Text></button>
            </View>
          </View>
        </View>
      </View>)
  }

  /**
   * 产品类型选项点击处理方法
   * @param {*} e 
   */
  onProductTypeOptionTap(e) {
    const option = e.currentTarget.dataset.producttype;
    const optionObj = JSON.parse(option);
    // 更新选中 option 对象
    this.setState({
      selfSelectedProductType: optionObj,
    }, () => {
      // console.log('onProductTypeOptionTap selfSelectedProductType = ', this.state.selfSelectedProductType)
    });
  }

  /**
   * 职业选项点击处理方法
   * @param {*} e 
   */
  onJobOptionTap(e) {
    const option = e.currentTarget.dataset.job;
    const optionObj = JSON.parse(option);
    // 更新选中 option 对象
    this.setState({
      selfSelectedJob: optionObj,
    }, () => {
      // console.log('onJobOptionTap selfSelectedJob = ', this.state.selfSelectedJob)
    });
  }

  /**
   * 抵押类型选项点击处理方法
   * @param {*} e 
   */
  onMortgageTypeOptionTap(e) {
    const option = e.currentTarget.dataset.mortage;
    const optionObj = JSON.parse(option);
    // 更新选中 option 对象
    this.setState({
      slefSelectedMortgageType: optionObj,
    }, () => {
      // console.log('onMortgageTypeOptionTap slefMortgageType = ', this.state.slefSelectedMortgageType)
    });
  }

  /**
   * 重置选项
   */
  onReset() {
    this.setState({
      selfSelectedProductType: productTypeOptions[0],
      selfSelectedJob: jobsOptions[0],
      slefSelectedMortgageType: mortgageTypeOptions[0],
    }, () => {
      // console.log('onReset selfSelectedProductType = ', this.state.selfSelectedProductType)
      // console.log('onReset selfSelectedJob = ', this.state.selfSelectedJob)
      // console.log('onReset slefSelectedMortgageType = ', this.state.slefSelectedMortgageType)
    })
  }

  /**
   * 提交表单方法
   * @param {*} e 
   */
  onSubmit(e) {
    // 发射自定义事件
    Taro.eventCenter.trigger(`${Constants.FILTER_SIDE_MENU_SELECT_EVENT}${this.props.eventSuffix}`, {
      selectedProductType: this.state.selfSelectedProductType,
      selectedjob: this.state.selfSelectedJob,
      selectedMrotageType: this.state.slefSelectedMortgageType,
      isShow: false,
    });
  }

  /**
   * 关闭过滤组件方法
   */
  onClose() {
    // 发射自定义事件
    Taro.eventCenter.trigger(`${Constants.FILTER_SIDE_MENU_SELECT_EVENT}${this.props.eventSuffix}`, {
      // selectedProductType: this.state.selfSelectedProductType,
      // selectedjob: this.state.selfSelectedJob,
      // selectedMrotageType: this.state.slefMortgageType,
      isShow: false,
    });
  }
}

export default ProductFilterSideMenu as ComponentClass<PageOwnProps, PageState>
