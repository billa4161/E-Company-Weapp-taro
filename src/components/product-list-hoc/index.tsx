import { ComponentClass, ComponentType } from 'react'
import Taro, { Component } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'
import { connect } from '@tarojs/redux'

import ProductType from '../../model/product'
import ProductList  from '../product-list'
import Pagination from '../pagination'
import Constants from '../../config/constants'
import { Utils } from '../../utils/utils'

import './index.scss'

type PageStateProps = {
  pageSize: number,
  userApplyList: any[],
}

type PageDispatchProps = {}

type PageOwnProps = {
  // 搜索条件
  searchCondition: any,
  // 是否要显示勾选框
  isShowCheck: boolean,
  // 是否要显示放款时间
  isShowLoanTime: boolean,
  // 是否要显示申请按钮
  isShowApply: boolean,
  // 是否要显示取消收藏
  isShowCollectCancel: boolean,
  // 没有加载到数据的提示语
  noneResultTips?: string,
}

type PageState = {
  // 事件名后缀，用于区分不同组件独立事件
  eventSuffix: string,
  // 数据是否加载中的标志位
  loading: boolean,
  // 分页加载的数据
  dataArray: ProductType[],
  // 数据总数
  total: number,
  // 没有搜索到结果的标志位  true:表示没有搜索到数据
  noneResult: boolean,
  // 没有更多搜索结果的提示语
  noneResultTips: string,
  // 展示用的数据列表
  allDataArray: ProductType[],
}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps

interface ProductListHoc {
  props: IProps;
  state: PageState;
}

const utils = new Utils()

/**
 * 产品列表的高阶组件，
 * 展示产品列表
 * 有分页功能，可以懒加载产品数据
 */
@connect(({ common }) => ({
  pageSize: common['defaultPageSize'],
  userApplyList: common['userApplyList'],
}), (dispatch) => ({}))
 class ProductListHoc extends Component {

  constructor(props) {
    super(props);

    this.state = Object.assign({}, {
      eventSuffix: `_${utils.random(10)}`,
      loading: false,
      total: 0,
      dataArray: [],
      allDataArray: [],
      noneResult: false,
      noneResultTips: ''
    })
  }
  
  componentDidMount() {
    this.initEventHandler()

    setTimeout(() => {
      // console.log('产品列表HOC组件 发送 PAGINATION_PAGE_RELOAD_EVENT')
      // Taro.eventCenter.trigger(`${Constants.PAGINATION_PAGE_RELOAD_EVENT}${this.state.eventSuffix}`)

      this.setState({
        dataArray: [0,0,0,0,0,0,0,0,0,0],
        loading: true,
        total: 20,
      })
    }, 1000);
    
  }

  componentWillReceiveProps(nextProps) {

  }

  componentWillUnmount() {

  }

  componentDidShow() { }

  componentDidHide() { }

  render() {
    
    return (
      <View className="product-list-hoc-container">
        <Pagination 
          dataArray={this.state.dataArray} 
          total={this.state.total} 
          pageSize={this.props.pageSize}
          eventSuffix={this.state.eventSuffix}
          loading={this.state.loading}/>
        <ProductList 
          dataArray={ this.state.allDataArray }
          isShowCheck={ this.props.isShowCheck } isShowLoanTime={ this.props.isShowLoanTime } isShowApply={ this.props.isShowApply } isShowCollectCancel={ this.props.isShowCollectCancel }
          loading={ this.state.loading }
          noneResult={ this.state.noneResult }
          noneResultTips={ this.state.noneResultTips }
          userApplyList={ this.props.userApplyList }/>
      </View >)
  }

  initEventHandler() {
    const that = this
    Taro.eventCenter.on(`${Constants.PAGINATION_PAGE_UPDATE_EVENT}${this.state.eventSuffix}`, (param: any) => {
      console.log('产品列表HOC组件 接收到 PAGINATION_PAGE_UPDATE_EVENT, para = ', param)
      that._updatePageData(param)
    })
  }

  removeEventHandler() {
    Taro.eventCenter.off(`${Constants.PAGINATION_PAGE_UPDATE_EVENT}${this.state.eventSuffix}`)
  }

  /**
   * 更新分页数据
   * @param param 分页参数
   */
  _updatePageData(param: any) {
    const { nextPage, dataArray,  noneResult, noneResultTips } = param
    this.setState({
      nextPage,
      allDataArray: dataArray,
      noneResult,
      noneResultTips,
    })
  }

}

export default ProductListHoc as ComponentClass<PageOwnProps, PageState>

