import { ComponentClass } from 'react'
import Taro, { Component } from '@tarojs/taro'
import { View, Text, Image } from '@tarojs/components'
import { connect } from '@tarojs/redux'

import './index.scss'

import radioImg from '../../asset/images/agree-radio/radio-default.png'
import radioCheckedImg from '../../asset/images/agree-radio/radio-checked.png'

type PageStateProps = { }

type PageDispatchProps = {
  dispatchIsAgreeProtocol: (type: string, payload: { isAgreeProtocol: boolean }) => void
}

type PageOwnProps = {
  isAgreeProtocol: boolean,   // 是否同意
  dispatchType: string,       // action名字符串
}

type PageState = {}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps

interface AgreeProtocolRadio {
  props: IProps;
}

/**
 * 图片按钮组件
 */
@connect(({ }) => ({
}), (dispatch) => ({
  dispatchIsAgreeProtocol(type: string, payload: { isAgreeProtocol: boolean }) {
    dispatch({
      type,
      payload,
    })
  },
}))
class AgreeProtocolRadio extends Component {

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }

  render() {
    return (
      <View className="radio-box">
        <Image className="radio-img" src={this.props.isAgreeProtocol ? radioCheckedImg : radioImg} onClick={this.onProtocolRadioTap}></Image>
        <Text className="radio-label text" onClick={this.onProtocolRadioTap}>我同意</Text>
        <Text className="protocol-color text" onClick={this.goToTermsOfService}>《壹融时代服务条款》</Text>
      </View>
    )
  }

  /**
   * 协议同意按钮点击处理方法
   * @param {*} e 
   */
  onProtocolRadioTap = (e) => {
    const { isAgreeProtocol } = this.props

    // 修改参数
    this.props.dispatchIsAgreeProtocol(this.props.dispatchType, {
      isAgreeProtocol: !isAgreeProtocol,
    })
  }

  /**
   * 跳转到服务条款
   * @param {*} e 
   */
  goToTermsOfService(e) {
    // 跳转到产品过滤列表页面
    Taro.navigateTo({
      url: '/pages/termOfService/index'
    })
  }
}

// #region 导出注意
//
// 经过上面的声明后需要将导出的 Taro.Component 子类修改为子类本身的 props 属性
// 这样在使用这个子类时 Ts 才不会提示缺少 JSX 类型参数错误
//
// #endregion

export default AgreeProtocolRadio as ComponentClass<PageOwnProps, PageState>
