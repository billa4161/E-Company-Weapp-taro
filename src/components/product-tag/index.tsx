import { ComponentClass } from 'react'
import Taro, { Component } from '@tarojs/taro'
import { View, Text, Image, Button } from '@tarojs/components'

import './index.scss'

type PageStateProps = {}

type PageDispatchProps = {}

type PageOwnProps = {
  styleClass?: any,   // 样式对象
  text: string, // 显示文本
}

type PageState = {}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps

interface ProductTag {
  props: IProps;
}

/**
 * 图片按钮组件
 */
class ProductTag extends Component {

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }

  public static defaultProps: PageOwnProps = {
    styleClass: {
      color: '#666666',
    },
    text: 'tag',
  }

  render() {
    return (
      <View className='product-tag-container' style={this.props.styleClass}>
          <Text>{ this.props.text }</Text>
      </View>
    )
  }
}

// #region 导出注意
//
// 经过上面的声明后需要将导出的 Taro.Component 子类修改为子类本身的 props 属性
// 这样在使用这个子类时 Ts 才不会提示缺少 JSX 类型参数错误
//
// #endregion

export default ProductTag as ComponentClass<PageOwnProps, PageState>
