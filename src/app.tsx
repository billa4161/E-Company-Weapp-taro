import '@tarojs/async-await'
import Taro, { Component, Config } from '@tarojs/taro'
import { Provider } from '@tarojs/redux'

import Home from './pages/home/index'
import configStore from './store'
import dva from './utils/dva'
import models from './dva-models'

import './app.scss'

// 如果需要在 h5 环境中开启 React Devtools
// 取消以下注释：
// if (process.env.NODE_ENV !== 'production' && process.env.TARO_ENV === 'h5')  {
//   require('nerv-devtools')
// }

// const store = configStore()

const dvaApp = dva.createApp({
  initialState: {},
  models: models,
});
const store = dvaApp.getStore();
console.log('store = ', store)

// 查询用户位置
setTimeout(() => {
  store.dispatch({
    type: 'common/refreshUserLocation',
    payload: {},
  })
}, 0)

class App extends Component {

  /**
   * 指定config的类型声明为: Taro.Config
   *
   * 由于 typescript 对于 object 类型推导只能推出 Key 的基本类型
   * 对于像 navigationBarTextStyle: 'black' 这样的推导出的类型是 string
   * 提示和声明 navigationBarTextStyle: 'black' | 'white' 类型冲突, 需要显示声明类型
   */
  config: Config = {
    pages: [
      'pages/home/index',
      'pages/stepApplyOne/index',
      'pages/stepApplyResult/index',
      'pages/stepApplyTwo/index',
      'pages/stepApplyMatch/index',
      'pages/termOfService/index',
      'pages/login/index',
      'pages/my/index',
      'pages/shop/index',
      'pages/index/index',
      'pages/bankProductDetail/index',
      'pages/bankProductApply/index',
      'pages/bankProductResult/index',
      'pages/fastProductApply/index',
      'pages/myCollection/index',
      'pages/myApply/index',
      'pages/productFilterList/index',
      'pages/myMoney/index',
      'pages/myFriend/index',
      'pages/myProfile/index',
      'pages/pinganBonus/index',
    ],
    window: {
      navigationBarBackgroundColor: '#4e8df7',
      navigationBarTitleText: '壹融时代',
      navigationBarTextStyle: "white",
      navigationStyle: 'defaul',
      backgroundColor: '#f3f3f3',
      enablePullDownRefresh: false
    },
    tabBar: {
      list: [{
        pagePath: "pages/home/index",
        text: "首页",
        iconPath: "./asset/images/tab-bar/home.png",
        selectedIconPath: "./asset/images/tab-bar/home@active.png"
      }, {
        pagePath: "pages/shop/index",
        text: "贷款商城",
        iconPath: "./asset/images/tab-bar/shop.png",
        selectedIconPath: "./asset/images/tab-bar/shop@active.png"
      },{
        pagePath: "pages/my/index",
        text: "我的",
        iconPath: "./asset/images/tab-bar/my.png",
        selectedIconPath: "./asset/images/tab-bar/my@active.png"
      }],
      selectedColor: "#4d81f6",
      backgroundColor: "#fff",
      color: "#9f9f9f",
      borderStyle: "black",
    }
  }

  componentDidMount () {}

  componentDidShow () {}

  componentDidHide () {}

  componentCatchError () {}

  componentDidCatchError () {}

  // 在 App 类中的 render() 函数没有实际作用
  // 请勿修改此函数
  render () {
    return (
      <Provider store={store}>
      </Provider>
    )
  }
}

Taro.render(<App />, document.getElementById('app'))
