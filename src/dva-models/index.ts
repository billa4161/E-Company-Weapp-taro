import common from './common'
import home from '../pages/home/model'
import shop from '../pages/shop/model'
import login from '../pages/login/model'
import stepApplyOne from '../pages/stepApplyOne/model'
import stepApplyTwo from '../pages/stepApplyTwo/model'
import stepApplyMatch from '../pages/stepApplyMatch/model'
import bankProductDetial from '../pages/bankProductDetail/model'
import bankProductApply from '../pages/bankProductApply/model'
import fastProductApply from '../pages/fastProductApply/model'
import my from '../pages/my/model'
import myCollection from '../pages/myCollection/model'
import myApply from '../pages/myApply/model'
import productFilterList from '../pages/productFilterList/model'
import myMoney from '../pages/myMoney/model'
import myFriend from '../pages/myFriend/model'
import myProfile from '../pages/myProfile/model'

export default [
  common,
  home,
  shop,
  login,
  stepApplyOne,
  stepApplyTwo,
  stepApplyMatch,
  bankProductDetial,
  bankProductApply,
  fastProductApply,
  my,
  myCollection,
  myApply,
  productFilterList,
  myMoney,
  myFriend,
  myProfile,
]